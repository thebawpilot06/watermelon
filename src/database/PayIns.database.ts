// PayIn interface
export type PayInDB = {
    id : number;
    // eslint-disable-next-line camelcase
    wallet_id : number;
    amount :number;
}

export type PayInsType = {
    // eslint-disable-next-line camelcase
    [pay_in_id : number] : PayInDB;
}

// eslint-disable-next-line import/no-mutable-exports,import/prefer-default-export,prefer-const
export let PayIns: PayInsType = { };
