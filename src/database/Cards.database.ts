// card interface

export enum BrandName {
    // eslint-disable-next-line no-unused-vars
    MasterCard = "master_card",
    // eslint-disable-next-line no-unused-vars
    Visa = "visa",
    AmericanExpress = "american_express",
    UnionPay = "union_pay",
    JCB = "jcb",
}

export type CardDB = {
    id: number;
    // eslint-disable-next-line camelcase
    last_four: string;
    brand: BrandName,
    // eslint-disable-next-line camelcase
    expired_at: Date,
    // eslint-disable-next-line camelcase
    user_id: number;
}

export type CardsDB = {
    [id: string]: CardDB;
}


export const Cards: CardsDB = { };
