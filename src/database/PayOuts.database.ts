/* eslint-disable camelcase */
// PayOut interface
export type PayOutDB = {
    id : number;
    wallet_id : number;
    amount : number;
}

export type PayOutsType ={
    [pay_out_id : number] : PayOutDB;
}

// eslint-disable-next-line import/prefer-default-export,prefer-const,import/no-mutable-exports
export let PayOuts : PayOutsType = { };
