/* eslint-disable camelcase */
// Users Interface
export type UserDB = {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    password: string;
    is_admin: boolean
};

type UsersType = {
    [useriD: string]: UserDB;
}

// eslint-disable-next-line import/prefer-default-export,prefer-const,import/no-mutable-exports
export let Users: UsersType = { };
