/* eslint-disable camelcase */
// Transfer interface

export type TransferDB = {
    id : number;
    debited_wallet_id : number;
    credited_wallet_id : number;
    amount : number;
}

export type TransfersType ={
    [transfert_id : number] : TransferDB;
}

// eslint-disable-next-line import/prefer-default-export,prefer-const,import/no-mutable-exports
export let Transfers : TransfersType = { };
