/* eslint-disable camelcase */
// Wallets interface

export type WalletDB = {
    id : number;
    user_id : number;
};

export type WalletsType = {
    [wallet_id : number] : WalletDB;
};

// eslint-disable-next-line import/prefer-default-export,prefer-const,import/no-mutable-exports
export let Wallets: WalletsType = { };
