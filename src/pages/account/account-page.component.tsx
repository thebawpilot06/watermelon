import React from "react";

/* Components */
// Styles
import { AccountPageS } from "./account-pagee.styles";

// Own
import AccountManager from "../../components/account/account-manager/account-manager.component";

const AccountPage: React.FunctionComponent = () => (
    <AccountPageS>
        <AccountManager />
    </AccountPageS>
);

export default AccountPage;
