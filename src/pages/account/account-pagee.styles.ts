import styled from "styled-components";

// Page style
import pageCss from "../../ressources/styles/page.styles";

export const AccountPageS = styled.div`
    ${pageCss};
    display: flex;
    justify-content: center;
    align-items: center;
`