import styled from "styled-components";

export const AuthenticationPageS = styled.div`
    width: 850px;
    display: flex;
    justify-content: space-between;
    margin: 60px auto;
    margin-bottom: 0;
`;

export const LogoContainerS = styled.div`
    position: absolute;
    right: 0;
    bottom: 0;
    overflow: hidden;
    height: 700px;
    width: 700px;
    z-index: -1;

    & svg {
        position: relative;
        left: 40%;
        top: 39%;
        transform: rotate(190deg);
    }
`;
