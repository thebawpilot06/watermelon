/* React */
import React, { Fragment } from "react";

/* Components */

// Logo
import { ReactComponent as WatermelonLogo } from "../../ressources/assets/watermelon-slice.svg";

// Styles
import {
    AuthenticationPageS,
    LogoContainerS,
} from "./authentication-page.styles";

// Own
import SignIn from "../../components/authentication/sign-in/sign-in.component";
import SignUp from "../../components/authentication/sign-up/sign-up.component";

const AuthenticationPage: React.FunctionComponent = () => (
    <Fragment>
        <AuthenticationPageS>
            <SignIn />
            <SignUp />
        </AuthenticationPageS>
        <LogoContainerS>
            <WatermelonLogo />
        </LogoContainerS>
    </Fragment>
);

export default AuthenticationPage;
