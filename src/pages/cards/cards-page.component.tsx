import React from "react";

/* Redux */
import { connect } from "react-redux";

// Action
import { fetchCardsRequest } from "../../redux/cards/cards.actions";

/* Components */

// Style
import CardsPageS from "./cards-page.styles";

// Own
import CardsControllerContainer from "../../components/cards/cards-controller/cards-controller.container";

/* Interfaces */
interface CardsPageActionProps {
	fetchCardsRequest: Function;
}

class CardsPage extends React.Component<CardsPageActionProps, {}> {
	componentDidMount() {
		this.props.fetchCardsRequest();
	}

	render() {
		return (
			<CardsPageS>
				<CardsControllerContainer />
			</CardsPageS>
		);
	}
}

export default connect(
	null,
	{ fetchCardsRequest },
)(CardsPage);
