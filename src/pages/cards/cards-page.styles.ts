import styled from "styled-components";

/* Page Data */
import pageCss from "../../ressources/styles/page.styles";

const CardsPageS = styled.div`
    ${pageCss}
`;

export default CardsPageS;
