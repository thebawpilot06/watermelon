import styled, { css } from "styled-components";

/* Styles */

// Page
import pageCss from "../../ressources/styles/page.styles";

// Box
import boxCss from "../../ressources/styles/box.styles";

const shadowProperties = css`
    
`;

export const HomePageS = styled.div`
    ${pageCss}
    padding: 15vh 0;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
`;

export const SoldView = styled.div`
    position: relative;
    height: 180px;
    width: 500px;
    background-color: white;
    border-radius: ${boxCss.borderRadius};
    display: flex;
    justify-content: center;
    align-items: center;
    ${shadowProperties};
`;
