import React from "react";

/* Redux */
import { connect } from "react-redux";

// Actions
import { fetchWalletDataRequest } from "../../redux/wallet/wallet.actions";

/* Components */

// Styles
import { HomePageS, SoldView } from "./home-page.styles";

// Own
import SoldContainer from "../../components/home/sold/sold.container";
import WalletOperations from "../../components/home/wallet-operations/wallet-operations.component";
import BrandSelector from "../../components/cards/brand-selector/brand-selector.component";

/* Interfaces */
interface HomePageActionProps {
	fetchWalletDataRequest: Function;
}

class HomePage extends React.Component<HomePageActionProps> {
	componentDidMount() {
		this.props.fetchWalletDataRequest();
	}

	render() {
		return (
			<HomePageS>
				<SoldView>
					<SoldContainer />
				</SoldView>
				<WalletOperations />
				</HomePageS>
		);
	}
}

export default connect(null, { fetchWalletDataRequest })(HomePage);
