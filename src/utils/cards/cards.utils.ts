import { BrandName } from '../../database/Cards.database';

export const checkCardNumberLength = (cardNumber: string): boolean => {
    const cardNumberLengthRex = /[0-9]{4}/;

    return cardNumberLengthRex.test(cardNumber);
};

export const checkCardExpiration = (cardExpiration: string): boolean => {
    // On verifie tout d'abord qu'on a le bon format
    const expirationRegex = /0[1-9]|1[0-2] \/ [0-9]{2}/;

    if (!expirationRegex.test(cardExpiration)) return false;

    let [month, year] = cardExpiration.split(" / ");

    // On rajoute le fait que l'annee est de la forme 20 + X
    year = "20" + year;

    const today = new Date();
    const expirationDate = new Date(parseInt(year, 10), parseInt(month, 10));

    return expirationDate > today;
};

export const generateCardColor = () => {
    let green;
    let red;
    let blue;

    red = Math.floor(Math.random() * 256);
    green = Math.floor(Math.random() * 256);
    blue = Math.floor(Math.random() * 256);

    red = red.toString(16);
    green = green.toString(16);
    blue = blue.toString(16);

    if (red.length === 1) { red = "0" + red; }
    if (green.length === 1) { green = "0" + green; }
    if (blue.length === 1) { blue = "0" + blue; }

    return "#" + red + green + blue;
};

export const getCardNumberFormatted = (cardNumber: string) => {
    let firstBlock;
    let secondBlock;
    let thirdBlock;
    let fourthBlock;

    firstBlock = cardNumber.slice(0, 4);
    secondBlock = cardNumber.slice(4, 8);
    thirdBlock = cardNumber.slice(8, 12);
    fourthBlock = cardNumber.slice(12, 16);

    return `${firstBlock}  ${secondBlock}  ${thirdBlock}  ${fourthBlock}`;
};