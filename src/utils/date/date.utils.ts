/* eslint-disable prefer-const */
import {
    format,
    parseISO,
    addMinutes,
    compareAsc,
} from "date-fns";

export const convertDateToMMYY = (date: Date | string) => {
    if (date instanceof Date) {
        return format(date, "MM / yy");
    }
    return format(parseISO(date), "MM / yy");
};

export const convertMMYYToDate = (date: string) => {
    let [month, year] = date.split(" / ");

    year = `20${year}`;

    const newDate = new Date(`${month}/01/${year}`);

    return newDate;
};

export const setExpiration = (minutes: number) => {
    const now = new Date();

    return addMinutes(now, minutes);
};

export const checkExpirationPassed = (expiration: Date | null) => {
    // On verifie tout d'abord si il y a une expiration sinon on considere comme si c'etait passe
    if (!expiration) return 1;

    const now = new Date();

    return compareAsc(now, expiration!) === 1;
};