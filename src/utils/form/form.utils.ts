export const checkUserName = (name: string) => {
	const userNameRegex = /^[A-Z][-'a-zA-Z]+,?\s[A-Z][-'a-zA-Z]{1,19}$/;

	return userNameRegex.test(name);
};

export const checkEmail = (email: string) => {
	// eslint-disable-next-line no-useless-escape
	const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	return emailRegex.test(email);
};

// eslint-disable-next-line arrow-body-style
export const checkPassword = (password: string) => {

	/*
     Expression reguliere permettant de verifier que le password possede au minimum 8 characteres.
     Il faudra egalement que le mot de passe possede au moins une lettre minuscule et au moins
     une majuscule. En plus de cela, il faudra egalement qu'il renseigne au moins un digit et un
     caractere special de type $ @ % * + - _ !
     On ne rigole pas sur la securite.
    */
	return /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%_])([-+!*$@%_\w]{8,15})$/.test(password);
};

// eslint-disable-next-line max-len
export const checkPasswordsMatch = (password: string, confirmPassword: string) => password === confirmPassword;

export const containsOnlyDigit = (inputValue: string, particularSymbol?: string) => {
    const regexExpression = particularSymbol ? new RegExp(particularSymbol!, "g") : "";
    const inputValueWithoutSymbol = inputValue.replace(regexExpression, "");

	// eslint-disable-next-line no-var
    var reg = /^\d{0,}$/;

    return reg.test(inputValueWithoutSymbol);
};
