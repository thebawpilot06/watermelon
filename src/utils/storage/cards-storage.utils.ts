const createCardsColorStorage = () => {
    window.localStorage.setItem("cards-color", JSON.stringify({}));
};

export const getColorForCard = (cardId: number): string => {
    // On verifie tout d'abord que l'objet existe
    if (!window.localStorage.getItem("cards-color")) {
        createCardsColorStorage();
    }

    const cardsColor = JSON.parse(window.localStorage.getItem("cards-color") as string);

    return cardsColor[cardId];
};

export const saveCardColor = (cardId: number, color: string) => {
    if (!window.localStorage.getItem("cards-color")) {
        createCardsColorStorage();
    }

    const cardsColor = JSON.parse(window.localStorage.getItem("cards-color") as string);

    cardsColor[cardId] = color;

    // On enregistre
    window.localStorage.setItem("cards-color", JSON.stringify(cardsColor));
};

export const removeCardColor = (cardId: number) => {
    const cardsColor = JSON.parse(window.localStorage.getItem("cards-color") as string);

    delete cardsColor[cardId];

    // On enregistre
    window.localStorage.setItem("cards-color", JSON.stringify(cardsColor));
};
