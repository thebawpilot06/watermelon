export const saveUserAvatar = (userId: number, file: string) => {
    let usersAvatar: {[userId: string]: string}
        = JSON.parse(window.localStorage.getItem("users_avatar") as string);

    if (!usersAvatar) {
        usersAvatar = {};
    }

    usersAvatar[userId] = file;

    window.localStorage.setItem("users_avatar", JSON.stringify(usersAvatar));
};

export const getAvatarForUser = (userId: number) => {
    const usersAvatar: {[userId: string]: string}
        = JSON.parse(window.localStorage.getItem("users_avatar") as string);

    return usersAvatar[userId];
};
