/* eslint-disable no-unused-vars,arrow-parens,arrow-body-style */
// Transfers
import {
    TransferDB,
    Transfers,
} from "../../database/Transfers.database";

/* Utils */
import { saveDocumentToLocalStorage } from "./database.utils";

export const addTransfert = (walletIdSender: number, walletIdReceiver: number, amount: number) => {
    // On choisit l'id du nouveau payin
    const id = Object.keys(Transfers).length + 1;
    const newTransfer: TransferDB = {
        id,
        amount,
        debited_wallet_id: walletIdSender,
        credited_wallet_id: walletIdReceiver,
    };

    Transfers[id] = newTransfer;

    // On va maintenant sauvegarder la base de donnees
    saveDocumentToLocalStorage("transfers", Transfers);
};

export const getCreditedTransfersFor = (walletId: number): TransferDB[] => {
    return Object.values(Transfers).filter(transfer => transfer.credited_wallet_id === walletId);
};

export const getDebitedTransfersFor = (walletId: number): TransferDB[] => {
    return Object.values(Transfers).filter(transfer => transfer.debited_wallet_id === walletId);
};
