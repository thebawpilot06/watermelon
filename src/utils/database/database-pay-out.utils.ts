/* eslint-disable no-unused-vars */
// PayOutsDb
import {
    PayOutDB,
    PayOuts,
} from "../../database/PayOuts.database";

/* Utils */
import { saveDocumentToLocalStorage } from "./database.utils";

export const addPayOutFor = (walletId: number, amount: number) => {
    // On choisit l"id du nouveau payout
    const id = Object.keys(PayOuts).length + 1;
    const newPayOut: PayOutDB = {
        id,
        wallet_id: walletId,
        amount,
    };

    PayOuts[id] = newPayOut;

    // On va maintenant sauvegarder la base de donnees
    saveDocumentToLocalStorage("payouts", PayOuts);
};

// eslint-disable-next-line arrow-body-style
export const getPayOutsFor = (walletId: number): PayOutDB[] => {
    // eslint-disable-next-line arrow-parens
    return Object.values(PayOuts).filter(payout => payout.wallet_id === walletId);
};
