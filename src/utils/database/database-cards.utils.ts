/* eslint-disable no-unused-vars,implicit-arrow-linebreak,arrow-parens */
// On importe notre table card
import { values } from "lodash";
import { CardDB, Cards, BrandName } from "../../database/Cards.database";

/* Utils */
// Db
import { saveDocumentToLocalStorage } from "./database.utils";

// CardColor
import { removeCardColor } from "../storage/cards-storage.utils";

// Date
import { convertDateToMMYY } from "../../utils/date/date.utils";

// Lodash
import { isEmpty } from "lodash";

export const getCardsFor = (userId: number): CardDB[] =>
    values(Cards).filter((card) => card.user_id === userId);
    

export const addCardToDb = (
	last_four: string,
	brand: BrandName,
	expiration: Date,
	user_id: number
): CardDB => {
	//On doit recuperer le nouvel id de la carte
	const keys = Object.keys(Cards);
	const newCardId = isEmpty(keys) ? 1 : parseInt(keys[keys.length -1]) + 1;

	const newCard: CardDB = {
		id: newCardId,
		last_four,
		brand,
		expired_at: expiration,
		user_id,
	};

	Cards[newCardId] = newCard;

	// On enregistre la base de donnees
	saveDocumentToLocalStorage("cards", Cards);

	return newCard;
};

export const editCardInDb = (
	cardId: number,
	last_four: string,
	brand: BrandName,
	expiration: Date
) => {
	const editedCard: CardDB = {
		...Cards[cardId],
		last_four,
		brand,
		expired_at: expiration,
	};

	Cards[cardId] = editedCard;

	saveDocumentToLocalStorage("cards", Cards);
};

export const removeCardFromDb = (cardId: number) => {
	delete Cards[cardId];

	// On enregistre la base de donnees
	saveDocumentToLocalStorage("cards", Cards);

	// On supprime egalement la couleur de la carte dans le storage
	removeCardColor(cardId);
};

/**
 * Fonction qui permet de savoir si une carte deja dans la base de donnees ou non
 */
export const checkCardExistence = (
    userId: number,
	cardLastFour: string,
	cardBrand: BrandName,
	expiration: Date
): Promise<boolean> => {
   
	const indexCard = Object.values(Cards).findIndex(
		(card) => {
            const dateExpirationDB = convertDateToMMYY(card.expired_at);
            const incomingDateExpiration = convertDateToMMYY(expiration);

            return card.user_id === userId && card.last_four === cardLastFour &&
            card.brand === cardBrand && dateExpirationDB === incomingDateExpiration;
        }
	);
    
	if (indexCard >= 0) return Promise.reject(new Error("CardInDb"));

	return Promise.resolve(false);
};
