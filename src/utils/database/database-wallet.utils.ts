/* eslint-disable no-unused-vars,arrow-parens,arrow-body-style */
import { isEmpty } from "lodash";

// WalletDb
import {
    WalletDB,
    Wallets,
} from "../../database/Wallets.databse";

/* Utils */
import { saveDocumentToLocalStorage } from "./database.utils";

export const createWalletFor = (userId: number) => {
    // On choisit l'id du nouveau wallet
    const id = Object.keys(Wallets).length + 1;
    const newWallet: WalletDB = {
        id,
        user_id: userId,
    };

    Wallets[id] = newWallet;

    // On va maintenant sauvegarder la base de donnees
    saveDocumentToLocalStorage("wallets", Wallets);
};


export const getWalletFor = (userId: number): WalletDB => {
    return Object.values(Wallets).find(wallet => wallet.user_id === userId) as WalletDB;
};
