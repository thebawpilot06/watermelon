/* eslint-disable arrow-parens,arrow-body-style,no-unused-vars,no-shadow */
// Lodash
import values from "lodash/values";

// CryptoJs
import CryptoJs from "crypto-js";
// Documents
import {
    UserDB,
    Users,
} from "../../database/Users.database";

/* Utils */
import { saveDocumentToLocalStorage } from "./database.utils";

// Lang
import databaseErrorLang from "../../ressources/lang/error/database-error.lang.json";

// Interfaces
import { User } from "../../redux/user/user.types";

// Functions
const checkUserExistance = (email: string) : Promise<UserDB> => {
    return new Promise((resolve, reject) => {
        const user = values(Users).find(user => user.email === email);

        // On verifie si l'utilisateur existe dans la base de donnees
        if (!user) reject();

        // Si il existe on retourne ses donnees
        resolve(user!);
    });
};

/**
 * Fonction permettant de connecter un utilisateur
 * @param email : email de l'utilisateur
 * @param password : password de l'utilisateur
 * Cette fonction retourne une promesse d'utilisateur cad si
 * l'utilisateur existe on obtiendra ses donnees sinon une erreur
 * est renvoye
 */
export const logUser = (email: string, password: string): Promise<UserDB> => {
    return new Promise((resolve, reject) => {
        setTimeout(async () => {
            let user: UserDB;

            try {
                user = await checkUserExistance(email);

                // L'utilisateur est bien dans la base de donnees, on verifie maintenant le mdp
                if (CryptoJs.DES.decrypt(user!.password, "hugo").toString(CryptoJs.enc.Utf8) !== password) {
                    reject(databaseErrorLang.wrongPassword);
                }

                // Les informations donnees sont toutes correctes
                resolve(user!);

            } catch (err) {
                reject(databaseErrorLang.userInexistant);
            }
        }, 2000);
    });
};

// eslint-disable-next-line max-len
export const createUser = (displayName: string, email: string, password: string): Promise<UserDB> => {
    return new Promise((resolve, reject) => {
        setTimeout(async () => {
            // Avant de creer un utilisateur on verifie tout d'abord s'il n'existe pas deja.
            let user: UserDB;

            try {
                user = await checkUserExistance(email);

                // Si le code arrive ici, cela veut dire que l'utilisateur existe deja
                reject(databaseErrorLang.userAlreadyInDb);
            } catch (err) {
                // Si on tombe ici on peut creer l'utilisateur en base de donnees.
                const [firstName, lastName] = displayName.split(" ");

                const newUser: UserDB = {
                    id: Object.keys(Users).length + 1,
                    first_name: firstName,
                    last_name: lastName,
                    email,
                    password: CryptoJs.DES.encrypt(password, "hugo").toString(),
                    is_admin: false,
                };

                // On ajoute l'utilisateur a la base de donnees
                Users[newUser.id] = newUser;

                // On retourne les donnees de l'utilisateur
                resolve(newUser);
            }

            saveDocumentToLocalStorage("users", Users);
        }, 3000);
    });
};

export const getWatermelonUsersData = (): {id: number, email:string}[] => {
    // Pour les besoins de l'appli je ne renvois que l'id et l'email
    const usersDb: UserDB[] = Object.values(Users);

    return usersDb.map(user => {
        return {
            id: user.id,
            email: user.email,
        };
    });
};

export const updateDataFor = (
    userId: number, 
    firstName: string, 
    lastName: string, 
    email: string, 
    password: string
): UserDB => {
    const usersDb: UserDB[] = Object.values(Users);

    const userDataUpdated: UserDB = {
        ...Users[userId],
        first_name: firstName,
        last_name: lastName,
        email,
        password: CryptoJs.DES.encrypt(password, "hugo").toString(),
    };

    //On met a jour l'objet Users
    Users[userId] = userDataUpdated;

    saveDocumentToLocalStorage("users", Users);

    return userDataUpdated;
}
