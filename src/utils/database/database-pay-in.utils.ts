/* eslint-disable no-unused-vars,arrow-parens */
// PayInDb
import {
    PayInDB,
    PayIns,
} from "../../database/PayIns.database";

/* Utils */
import { saveDocumentToLocalStorage } from "./database.utils";

export const addPayIn = (walletId: number, amount: number) => {
    // On choisit l'id du nouveau payin
    const id = Object.keys(PayIns).length + 1;
    const newPayIn: PayInDB = {
        id,
        wallet_id: walletId,
        amount,
    };

    PayIns[id] = newPayIn;

    // On va maintenant sauvegarder la base de donnees
    saveDocumentToLocalStorage("payins", PayIns);
};

// eslint-disable-next-line arrow-body-style
export const getPayInsFor = (walletId: number): PayInDB[] => {
    return Object.values(PayIns).filter(payin => payin.wallet_id === walletId);
};
