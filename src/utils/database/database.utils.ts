// DbDocuments
import { Users } from "../../database/Users.database";

import { Wallets } from "../../database/Wallets.databse";

import { Cards } from "../../database/Cards.database";

import { PayIns } from "../../database/PayIns.database";

import { PayOuts } from "../../database/PayOuts.database";

import { Transfers } from "../../database/Transfers.database";

export const saveDocumentToLocalStorage = (documentName: string, db: object) => {
    window.localStorage.setItem(`database-${documentName}`, JSON.stringify(db));
};

export const reloadDbDocuments = () => {
    // On va tout d'abord verifier que le champ database-users existe dans le local storage
    if (!window.localStorage.getItem("database-users")) {
        saveDocumentToLocalStorage("users", {});
    }

    // Maintenant on peut associer le contenu present dans le local storage a l'object Users
    const usersDb = JSON.parse(window.localStorage.getItem("database-users") as string);
    Object.assign(Users, usersDb);

    // On va faire de meme pour les autres documents

    // Wallets
    if (!window.localStorage.getItem("database-wallets")) {
        saveDocumentToLocalStorage("wallets", {});
    }

    // Maintenant on peut associer le contenu present dans le local storage a l'object Users
    const walletsDb = JSON.parse(window.localStorage.getItem("database-wallets") as string);
    Object.assign(Wallets, walletsDb);

    // Cards
    if (!window.localStorage.getItem("database-cards")) {
        saveDocumentToLocalStorage("cards", {});
    }

    // Maintenant on peut associer le contenu present dans le local storage a l'object Users
    const cardsDb = JSON.parse(window.localStorage.getItem("database-cards") as string);
    Object.assign(Cards, cardsDb);


    // PayIns
    if (!window.localStorage.getItem("database-payins")) {
        saveDocumentToLocalStorage("payins", {});
    }

    // Maintenant on peut associer le contenu present dans le local storage a l'object Users
    const payInsDb = JSON.parse(window.localStorage.getItem("database-payins") as string);
    Object.assign(PayIns, payInsDb);

    // PayOuts
    if (!window.localStorage.getItem("database-payouts")) {
        saveDocumentToLocalStorage("payouts", {});
    }

    // Maintenant on peut associer le contenu present dans le local storage a l'object Users
    const payOutsDb = JSON.parse(window.localStorage.getItem("database-payouts") as string);
    Object.assign(PayOuts, payOutsDb);

    // Transfers
    if (!window.localStorage.getItem("database-transfers")) {
        saveDocumentToLocalStorage("transfers", {});
    }

    // Maintenant on peut associer le contenu present dans le local storage a l'object Users
    const transfersDb = JSON.parse(window.localStorage.getItem("database-transfers") as string);
    Object.assign(Transfers, transfersDb);
};
