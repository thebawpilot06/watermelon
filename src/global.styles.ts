import { createGlobalStyle } from "styled-components";

/* Colors */
import { Colors } from './ressources/styles/colors.styles';

export const GlobalStyle = createGlobalStyle`
	body {
        font-family: "Lora", sans-serif;
        font-size: 1.2rem;
		background-color: ${Colors.whiteBg};
		margin: 0;
		width: 100vw;
		height: 100vh;
	}
	

	.App {
		position: absolute;
		top:0;
		left: 0;
		height: 100vh;
		width: 100vw;
	}
		
	a {
		text-decoration: none;
		color: black;
    }
    
	* {
        box-sizing: border-box;
	}

	small {
		font-size: 0.6rem;
		font-style: italic;
	}
`;
