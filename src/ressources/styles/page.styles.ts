import { css } from "styled-components";

/* Header Data */
import { headerWidth } from "../../components/header/header.styles";

const pageWidth = 100 - headerWidth;

const pageCss = css`
    position: absolute;
    top: 0;
    left: ${headerWidth}vw;
    width: ${pageWidth}vw;
    height: 100vh;
    background-color: rgba(255, 254, 240, 0.6);
    ;
`;
export default pageCss;
