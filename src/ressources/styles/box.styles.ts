import { css } from "styled-components";

const boxCss = {
    borderRadius: "15px",
    shadow: css`
        -webkit-box-shadow: 6px 5px 12px -6px rgba(0,0,0,0.75);
        -moz-box-shadow: 6px 5px 12px -6px rgba(0,0,0,0.75);
        box-shadow: 6px 5px 12px -6px rgba(0,0,0,0.75);
    `,
};

export default boxCss;
