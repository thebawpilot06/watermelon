// eslint-disable-next-line import/prefer-default-export
export const Colors = {
	lightBlack: "#757575",
	black: "#171717",
	lightRed: "#E68364",
	red: "#f54e59",
	green: "#7dd15c",
	darkGreen: "#4DAF7C",
	lightGrey: "#EEEEEE",
	grey: "#4a4a4a",
	whiteBg: "#fffeeb",
	lightBlue: "#c2d2fc",
	orange: "#fcc06d",
	blue: "#4287f5",
	yellow: "#ebcf34",
};
