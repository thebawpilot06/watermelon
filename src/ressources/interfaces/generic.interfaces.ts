/* eslint-disable no-unused-vars */
import { BrandName } from "../../database/Cards.database";

export interface ObjectContent {
    value: string;
    error: boolean;
}

export interface CardData {
	cardId: number;
	cardNumber: string;
	cardExpiration: string;
	cardColor: string;
	cardBrand: BrandName;
}
