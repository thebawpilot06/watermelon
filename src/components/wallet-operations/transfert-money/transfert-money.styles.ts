import styled from "styled-components";

export const TransfertS = styled.div`
`;

export const TransfertHeaderS = styled.div`
    margin-bottom : 15px;
    font-family: 'Ubuntu', sans-serif;
    font-weight:bold;
    font-size: 1.4rem;
    text-align: center;
    padding-bottom : 15px;
`;

export const TransfertBodyS = styled.div`
    height : 200px;
    padding-bottom : 2px;
    padding-top : 15px;
    margin-left: 5px;
    margin-top : 5px;
    margin-bottom : 15px;
    text-align : center;
    font-family: 'Ubuntu', sans-serif;
    font-size: 1.4rem;
`;

export const TransfertFooterS = styled.div`
`;

export const ButtonArrowsS = styled.div`
    & .next-logo {
            height: 60px;
            width: 60px;
            cursor: pointer;
            position: absolute;
            right: 20px;
            top: 330px;
            padding-bottom : 10px;
            padding-top : 20px
        }
`;
