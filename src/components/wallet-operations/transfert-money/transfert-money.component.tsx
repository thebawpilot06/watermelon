import React from "react";

/* Redux */
import { connect } from "react-redux";
import {
  fetchWalletDataRequest,
  transfertRequest,
  // eslint-disable-next-line no-unused-vars
  transfertRequestType,
} from "../../../redux/wallet/wallet.actions";
import { getWatermelonUsersData } from "../../../utils/database/database-user.utils";
import { selectCurrentUser } from "../../../redux/user/user.selectors";
// eslint-disable-next-line no-unused-vars
import IRootState from "../../../redux/root/root.types";

/* Components */

/* styles */
import {
  TransfertHeaderS,
  TransfertBodyS,
  ButtonArrowsS,
  TransfertS,
  TransfertFooterS,
} from "./transfert-money.styles";

/* Own */
import Modal from "../../generic/modal/modal.component";
import SingleDataForm from "../single-data-form/single-data-form.component";
import { ReactComponent as Arrow } from "../../../ressources/assets/right-arrow.svg";
import SelectorController from "../../../components/selector/selector-controller/selector-controller.component";
import Notification, { NOTIFACTION_BG } from "../../generic/notification/notification.component";

/* Interface */

// Props redux : actions
interface TransfertActionProps {
  fetchWalletDataRequest: Function;
  transfertRequest: transfertRequestType;
}

// Variables du composant :
// - les emails des users (sauf celui courant) pour le selecteur d'users
// -un boolean pour savoir si un user a bien été selectionné
// -le mail de l'utilisateur selectionné
interface TransfertState {
  userSelected : string,
  isSomebody: boolean,
  emailUsers : string[],
}

// Props venant du composant parent : solde du compte
interface TransfertProps {
  walletAmount: number,
}

// Props redux : données ( les utilisateurs de l'appli)
interface TransfertUserReduxProps {
  users: {
    id: number,
    email: string,
  }[]
}

// Props redux : donnée : utilisateur courant
interface TransfertCurrentUserReduxProps {
  currentUser : {
    id: number,
    // eslint-disable-next-line camelcase
    first_name: string,
    // eslint-disable-next-line camelcase
    last_name: string,
    email: string,
    password: string,
    // eslint-disable-next-line camelcase
    is_admin: boolean,
    avatar?: string,
  } |null,
}


class Transfert extends React.Component<
  TransfertActionProps &
  TransfertProps &
  TransfertUserReduxProps &
  TransfertCurrentUserReduxProps,
  TransfertState> {
  constructor(props : TransfertActionProps &
    TransfertProps &
    TransfertUserReduxProps &
    TransfertCurrentUserReduxProps) {
    super(props);
    this.state = {
      userSelected: "",
      isSomebody: false,
      emailUsers: this.recupEmail(),
    };
  }

  componentDidMount() {
    this.props.fetchWalletDataRequest();
  }

  // on récupère tous les emails des utilisateurs, sauf celui de l'utilisateur courant
  // (il ne peut pas se transférer d'argent a lui-meme)
  recupEmail = () => {
    const emails = [];
    const currentEmail = [];
    const { users } = this.props;
    if (this.props.currentUser != null) {
      const { email } = this.props.currentUser;
      currentEmail.push(email);
    }

    // eslint-disable-next-line prefer-const
    for (let i in Object.keys(users)) {
      const { email } = users[i];
      if (email !== currentEmail[0]) {
        emails.push(email);
      }
    }
    return emails;
  };

  // on retrouve l'id de l'utilisateur selectionné, pour la requete transfertRequest
  findIdUser = () => {
    const { users } = this.props;
    const { userSelected } = this.state;
    // on parcours les users pour trouver l'email qui correspond et on retourn l'id
    for (const i in users) {
      const { id, email } = users[i];

      if (email === userSelected) {
        return id;
      }
    }
    return null;
  };

  // fonction qui recupère le montant selectionné dans le composant SingleDataForm après validation
  // on execute alors la requete transfertRequest avec l'id de l'user et le montant a transferer
  handleAmountReceive = (amountSelected : number) => {
    const idUser = this.findIdUser();
    if (idUser != null) {
      this.props.transfertRequest(idUser, amountSelected);

      // Petit message pour l'utilisateur
      Notification.instance.show(NOTIFACTION_BG.SUCCESS, "Virement envoyé 🛫");
    }
  };

  // lorsque l'utilisateur a cliqué sur le boutton next et si un utilisateur a bien été selectionné:
  // on ouvre un nouveau modal pour selectionné le montant a transferer.
  onClickNext = () => {
    if (this.state.isSomebody) {
      Modal.instance.configure(
        {
          headerContent: undefined,
          headerContentPos: undefined,
          headerTextColor: undefined,
          bodyContent:
            <SingleDataForm
              labelFormInput="Montant"
              labelHeader="Selectionner un montant à transferer :"
              onAmountSend={this.handleAmountReceive}
              isAmountCheck
              walletAmount={this.props.walletAmount}
            />,
          bodyTextColor: undefined,
          footerContent: undefined,
          footerContentPos: undefined,
          footerTextColor: undefined,
          hideCloseButton: false,
        },
      );
      Modal.instance.show();
    } else {
      Notification.instance.show(NOTIFACTION_BG.WARNING, "Veuillez selectionner une personne");
    }
  };

  // lorsqu'un user est selectionné, on met a jour le state
  onSelectItem = (item : string) => this.setState({
    userSelected: item,
    isSomebody: true,
  });

  render() {
    return (
      <TransfertS>
        <TransfertHeaderS>
         Faire un virement
        </TransfertHeaderS>
        <TransfertBodyS>
          <SelectorController
            onItemSelected={this.onSelectItem}
            items={this.state.emailUsers}
            placeHolderText="Sélectionner l'email de votre destinataire"
          />
        </TransfertBodyS>
        <TransfertFooterS>
          <ButtonArrowsS>
            <Arrow className="next-logo" onClick={this.onClickNext} />
          </ButtonArrowsS>
        </TransfertFooterS>
      </TransfertS>
    );
  }
}
const mapStateToProps = (state: IRootState) => ({
  users: getWatermelonUsersData(),
  currentUser: selectCurrentUser(state),
});

export default connect(
  mapStateToProps,
  {
    fetchWalletDataRequest,
    transfertRequest,
  },
)(Transfert);
