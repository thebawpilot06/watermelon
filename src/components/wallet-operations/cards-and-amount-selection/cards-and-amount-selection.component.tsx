import React from "react";

/* Redux */
import { connect } from "react-redux";

// Selector
import { selectCards } from "../../../redux/cards/cards.selectors";
import { selectWalletAmount } from "../../../redux/wallet/wallet.selectors";

// Actions
import {
  topUpRequest,
  // eslint-disable-next-line no-unused-vars
  topUpRequestType,
  withdrawRequest,
  // eslint-disable-next-line no-unused-vars
  withdrawRequestType,
} from "../../../redux/wallet/wallet.actions";

import { fetchCardsRequest } from "../../../redux/cards/cards.actions";


/* Components */
// Styles
import {
  CardsAndAmountSelectionS,
  CardsAndAmountSelectionHeaderS,
  CardsAndAmountSelectionBodyS,
  ButtonArrowsS,
} from "./cards-and-amount-selection.styles";

// Own
import Modal from "../../generic/modal/modal.component";
import SingleDataForm from "../single-data-form/single-data-form.component";
import { ReactComponent as Arrow } from "../../../ressources/assets/right-arrow.svg";
import CardsSelectorContainer
  from "../../../components/wallet-operations/cards-selector/cards-selector.container";

/* Interfaces */
// eslint-disable-next-line no-unused-vars
import { CardData } from "../../../ressources/interfaces/generic.interfaces";
// eslint-disable-next-line no-unused-vars
import IRootState from "../../../redux/root/root.types";
import Notification, { NOTIFACTION_BG } from "../../generic/notification/notification.component";

// Props venant de redux :actions
interface CardsAndAmountSelectionActionsProps {
  fetchCardsRequest: Function,
  topUpRequest: topUpRequestType,
  withdrawRequest : withdrawRequestType,
}

// Props venant de redux : données ( les cartes de l'utilisateur et le montant du solde)
interface CardsAndAmountSelectionReduxProps {
  cards: { [key: string]: CardData } | null;
  walletAmount : number,
}

//les variables du composant : le mois et l'année d'aujourdh'ui et un boolean,
// qui est modifié en fonction de si la date d'expi de la carte est passée ou non
interface CardsAndAmountSelectionState {
  month: number;
  year: number;
  expirationOK: boolean;
}

// Props venant du composant parent : quel action on va devoir faire (Withdraw ou topUp)
// et le texte du header
interface CardsAndAmountSelectionProps {
  actionToDo : string,
  headerText: string,
}

class CardsAndAmountSelection extends React.Component<
  CardsAndAmountSelectionActionsProps &
  CardsAndAmountSelectionReduxProps &
  CardsAndAmountSelectionProps,
  CardsAndAmountSelectionState> {
  constructor(props: CardsAndAmountSelectionActionsProps &
    CardsAndAmountSelectionReduxProps &
    CardsAndAmountSelectionProps) {
    super(props);
    this.state = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      expirationOK: false,
    };
  }

  componentDidMount() {
    this.props.fetchCardsRequest();
  }

  // on va vérifier l'expiration de la carte, par rapport à la date d'aujourd'hui
  checkExpiration = (cardSelected: CardData) => {
    // remise en forme de la date d'expi de la carte et conversion en nombre pour comparaison:
    const yearTmp = "20".concat(
      cardSelected.cardExpiration.split("/")[1].trim(),
    );
    const monthTmp = cardSelected.cardExpiration.split("/")[0].trim();
    const yearTmpNumber = parseInt(yearTmp, 10);
    const monthTmpNumber = parseInt(monthTmp, 10);

    // on compare les dates
    if (this.state.year < yearTmpNumber) {
      return true; // l'année est strictement inférieure, pas besoin de verifier le mois
    } // l'année est égale,on compare le mois (on considère que le mois en cours est valide)
    if (this.state.year === yearTmpNumber) {
        if (this.state.month <= monthTmpNumber) {
          return true;
        }
    }
    return false;
  };

  // si la selection de la carte change, on vérifie la date d'expiration
  handleOnCardChange = (cardSelected: CardData) => this.setState(
    { expirationOK: this.checkExpiration(cardSelected) },
    );

  // Fonction qui récupère le montant selectionné dans le composant enfant SingleDataForm
  // lorsque le bouton valider a été cliqué. On exécute la requete de TopUp
  handleAmountReceiveTopUp = (amountSelected: number) => {
    if (this.state.expirationOK) {
      this.props.topUpRequest(amountSelected);

      // Petit message pour l'utilisateur
      Notification.instance.show(NOTIFACTION_BG.SUCCESS, "🤑 Votre compte a été renfloué avec succés 🤑");
    }
  };

  // Fonction qui récupère le montant selectionné dans le composant enfant SingleDataForm
  // lorsque le bouton valider a été cliqué. On exécute la requete de Withdraw
  handleAmountReceiveWithdraw = (amountSelected: number) => {
    if (this.state.expirationOK) {
      this.props.withdrawRequest(amountSelected);

      // Petit message informatif pour l'utilisateur
      Notification.instance.show(NOTIFACTION_BG.SUCCESS, "Nous préparons le virement vers votre banque 🤗");
    }
  };

  /* lorsque l'utilisateur clique sur le boutton next, si la date d'expi est OK,
   on regarde dans quel composant on est, pour adapter les message et les actions a faire.
   on va passer dans le composant singleDataForm les texts a afficher et
   la fonction qui récupère le montant selectionné */
  onClickNext = () => {
    const labelheaderTopUp = "Selectionner le montant à ajouter :";
    const labelHeaderWithdraw = "Selectionner le montant à retirer :";
    const labelForm = "Montant";

    if (this.state.expirationOK) {
      if (this.props.actionToDo === "withdraw") { // si l'action a effectué est un withdraw
        Modal.instance.configure({
          bodyContent: (
            <SingleDataForm
              labelFormInput={labelForm}
              labelHeader={labelHeaderWithdraw}
              onAmountSend={this.handleAmountReceiveWithdraw}
              isAmountCheck
              walletAmount={this.props.walletAmount}
            />
          ),
          hideCloseButton: false,
        });
        Modal.instance.show();
      }

      if (this.props.actionToDo === "topup") { // si l'action a effectuée est un topUp
        Modal.instance.configure({
          bodyContent: (
            <SingleDataForm
              labelFormInput={labelForm}
              labelHeader={labelheaderTopUp}
              onAmountSend={this.handleAmountReceiveTopUp}
              isAmountCheck={false}
              walletAmount={0}
            />
          ),
          hideCloseButton: false,
        });
        Modal.instance.show();
      }
    } else {
      Notification.instance.show(NOTIFACTION_BG.WARNING, "Veuillez selectionner une carte");
    }
  };

  render() {
    const { cards } = this.props;
    return (
      <CardsAndAmountSelectionS>
        <CardsAndAmountSelectionHeaderS>
          <p>{this.props.headerText}</p>
        </CardsAndAmountSelectionHeaderS>
        <CardsAndAmountSelectionBodyS>
        <CardsSelectorContainer
          onCardChange={this.handleOnCardChange}
          cards={cards}
          cardValidationError={!this.state.expirationOK}
        />
        </CardsAndAmountSelectionBodyS>
        <ButtonArrowsS>
          <Arrow className="next-logo" onClick={this.onClickNext} />
        </ButtonArrowsS>
      </CardsAndAmountSelectionS>
    );
  }
}

const mapStateToProps = (state: IRootState): CardsAndAmountSelectionReduxProps => ({
  cards: selectCards(state),
  walletAmount: selectWalletAmount(state),
});

export default connect(
  mapStateToProps,
  {
    fetchCardsRequest,
    topUpRequest,
    withdrawRequest,
  },
)(CardsAndAmountSelection);
