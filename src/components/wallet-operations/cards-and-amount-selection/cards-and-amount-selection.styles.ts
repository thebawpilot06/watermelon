import styled from "styled-components";

export const CardsAndAmountSelectionS = styled.div`

`;

export const CardsAndAmountSelectionHeaderS = styled.div`
    margin-bottom : 20px;
    text-align: center;
    font-family: 'Ubuntu', sans-serif;
    font-weight:bold;
    font-size: 1.4rem;
`;

export const CardsAndAmountSelectionBodyS = styled.div`
    width: 700px;
    margin-top: 3rem;
    display: flex;
    flex-direction: row;
    justify-content: center;
`;

export const ButtonArrowsS = styled.div`
    & .next-logo {
            height: 60px;
            width: 60px;
            cursor: pointer;
            position: absolute;
            right: 20px;
            top: 330px;
            padding-bottom : 10px;
            padding-top : 20px
        }
`;
