import styled from "styled-components";

export const SingleDataFormS = styled.div`

`;

export const SingleDataFormFooterS = styled.div`
      padding-top : 30px;
`;

export const SingleDataFormHeaderS = styled.div`
    margin-bottom : 30px;
    font-family: 'Ubuntu', sans-serif;
    font-weight:bold;
    font-size: 1.4rem;
    padding-bottom : 30px;
`;

export const SingleDataFormBodyS = styled.div`
    width: 300px;
    height : 200px;
    padding-top : 15px;
    margin-left: 5px;
    margin-top : 5px;
    margin-bottom : 15px;
    text-align : center;
    font-family: 'Ubuntu', sans-serif;
    font-size: 1.4rem;
`;
