/* eslint-disable no-lonely-if */
import React from "react";

/* Components */

// styles
import {
  SingleDataFormHeaderS,
  SingleDataFormBodyS,
  SingleDataFormS,
  SingleDataFormFooterS,
} from "./single-data-form.styles";

/* Utils */
import { Colors } from "../../../ressources/styles/colors.styles";

// Own
import Modal from "../../generic/modal/modal.component";
import CustomButton from "../../generic/custom-button/custom-button.component";
import FormInput from "../../generic/form-input/form-input.component";
import Notification, { NOTIFACTION_BG } from "../../generic/notification/notification.component";


/* Interfaces */

/* Props venant du composant parent :
 - le texte du header, le text du label de l'input
 - un boolean qui nous indique si on doit vérifier le montant selectionné en fonction du solde :
 en effet, ce n'est pas nécessaire pour faire un topUp, contrairement au withdraw et au transfert
 -le montant du solde du compte */
interface SingleDataFormProps {
  labelFormInput : string,
  labelHeader : string,
  isAmountCheck : boolean,
  walletAmount :number,
}

/* Variables du composant :
 -le montant selectionné
 - un boolean qui indique si le montant choisi est OK par rapport au solde du compte
 - un boolean pour indique si une valeur a été saisie (pour le css : couleur de l'input) */
interface AmountState {
  amountSelected : number,
  walletSoldError : boolean,
  isValue: boolean,
}

// Props : function qui va renvoyer le montant selectionné au composant parent
interface AmountSelectedActionProps {
  onAmountSend(amount : number): void;
}

class SingleDataForm extends React.Component<
  SingleDataFormProps &
  AmountSelectedActionProps,
  AmountState> {
  constructor(props : SingleDataFormProps & AmountSelectedActionProps) {
    super(props);
    this.state = {
      amountSelected: 0,
      walletSoldError: false,
      isValue: false,
    };
  }

  // fonction qui vérifie si le montant séléctionné est bien inférieur au solde du compte
  checkAmount = (amount: number) => {
    if (amount <= this.props.walletAmount) {
      return false;
    }
    return true;
  };

  // fonction qui vérifie si une valeur a bien été entrée
  checkIsValue = (value : string) => {
    if (value !== "0") {
      return true;
    }
    return false;
  };

  // dès que la valeur de l'input change, on récupère la valeur,
  // on vérifie si elle est OK par rapport au solde
  onChangeValue =(ev: React.SyntheticEvent<HTMLInputElement>) => this.setState({
    amountSelected: parseInt(ev.currentTarget.value, 10),
    walletSoldError: this.checkAmount(parseInt(ev.currentTarget.value, 10)),
    isValue: this.checkIsValue(ev.currentTarget.value),
  });

  /* Après avoir cliqué sur le boutton valider :
   on regarde si il est necéssaire que le montant soit inférieur au solde :
   TopUp : non necéssaire, contrairement au withdraw et transfert
   si on n'a pas besoin de checker, on renvoie directement la valeur au composant parent
   si il faut checker, on vérifie le boolean qui indique si le montant est
   bien inférieur au solde avant d'envoyer le montant au composant parent
   On a aussi blinder le fait qu'il n'y ait aucune valeur */
  onClickValidation = () => {
    // eslint-disable-next-line no-restricted-globals
    if (!this.props.isAmountCheck && this.state.isValue && !isNaN(this.state.amountSelected)) {
      this.props.onAmountSend(this.state.amountSelected);
      Modal.instance.hide();
    } else {
      // on verifie si le montant de la transaction est bien inférieur au solde du compte
      // avant de l'envoyer au composant parent
      // eslint-disable-next-line no-restricted-globals
      if (!this.state.walletSoldError && this.state.isValue && !isNaN(this.state.amountSelected)) {
        this.props.onAmountSend(this.state.amountSelected);
        Modal.instance.hide();
      } else {
        // eslint-disable-next-line no-restricted-globals
          if (!this.state.isValue || isNaN(this.state.amountSelected)) {
            Notification.instance.show(NOTIFACTION_BG.WARNING, "vous n'avez pas selectionné de montant !");
        } else {
            Notification.instance.show(NOTIFACTION_BG.WARNING, "Le montant que vous avez selectionné est supérieur au solde de votre compte !");
          }
      }
    }
  };

  render() {
    return (
      <SingleDataFormS>
        <SingleDataFormHeaderS>
          <p>{this.props.labelHeader}</p>
        </SingleDataFormHeaderS>
        <SingleDataFormBodyS>
          {
            this.props.isAmountCheck ?
              (
                <FormInput
                handleChange={this.onChangeValue}
                label="Amount"
                type="number"
                value={this.state.amountSelected.toString(10)}
                displayWarningColor={this.state.walletSoldError}
                displayValidColor={!this.state.walletSoldError && this.state.isValue}
                />
              ) :
              (
                <FormInput
                handleChange={this.onChangeValue}
                label={this.props.labelFormInput}
                type="text"
                value={this.state.amountSelected.toString()}
                displayWarningColor={false}
                displayValidColor={this.state.isValue}
                />
              )
          }
        </SingleDataFormBodyS>
      <SingleDataFormFooterS>
        <CustomButton onClick={this.onClickValidation} color={Colors.green}>Valider</CustomButton>
      </SingleDataFormFooterS>
      </SingleDataFormS>
    );
  }
}

export default SingleDataForm;
