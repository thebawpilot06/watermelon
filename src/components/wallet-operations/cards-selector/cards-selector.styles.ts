import styled from "styled-components";

export const CardsSelectorS = styled.div`
    height: 250px;
    min-width: 450px;
    padding: 0 1rem;
    overflow: scroll;
    & .card {
        margin-bottom: 1.5rem;
    }

    & .card:last-of-type {
        margin-bottom: 0;
    }
`;

export const CardsSelectorListS = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: start;
    align-items: center;
`;

export const NoCardTextS = styled.div`
    text-align: center;
    font-family: 'Open Sans Condensed', sans-serif;
    font-size: 1.4rem;
`;
