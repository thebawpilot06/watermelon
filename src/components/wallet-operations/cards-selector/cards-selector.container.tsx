/* Redux */
import { connect } from "react-redux";

// Selector
import { selectAreCardsFetching } from "../../../redux/cards/cards.selectors";

/* Components */
// HOC
import WithSpinner from "../../spinner/with-spinner.component";

// Own
import CardsSelector, {CardsSelectorActionProps, CardsSelectorProps } from "./cards-selector.component";

/* Interfaces */
import IRootState from "../../../redux/root/root.types";

interface CardsSelectorContainerProps {
	isLoading: boolean;
}

const mapStateToProps = (state: IRootState): CardsSelectorContainerProps => ({
	isLoading: selectAreCardsFetching(state),
});

export default connect(
	mapStateToProps,
	{},
)(WithSpinner<CardsSelectorActionProps & CardsSelectorProps>(CardsSelector));
