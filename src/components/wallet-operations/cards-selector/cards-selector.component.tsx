import React from "react";

/* Components */

// Styles
import {
	CardsSelectorS,
	CardsSelectorListS,
	NoCardTextS,
} from "./cards-selector.styles";

// Own
import CardView from "../../cards/card-view/card-view.component";

/* Interfaces */
// eslint-disable-next-line no-unused-vars
import { CardData } from "../../../ressources/interfaces/generic.interfaces";

export interface CardsSelectorActionProps {
	onCardChange(card: CardData): void;
}

export interface CardsSelectorProps {
	cards: { [key: string]: CardData } | null;
	cardValidationError: boolean;
}

interface CardsSelectorState {
	selectedCardIndex: number | null;
}

class CardsSelector extends React.Component<
	CardsSelectorActionProps & CardsSelectorProps,
	CardsSelectorState
> {
	constructor(
		props: CardsSelectorActionProps &
			CardsSelectorProps,
	) {
		super(props);

		this.state = { selectedCardIndex: null };
	}

	handleCardChange = (card: CardData, index: number) => {
		this.setState({ selectedCardIndex: index });

		this.props.onCardChange(card);
	};

	renderCardsItem = () => {
		const { cards, cardValidationError } = this.props;
		const { selectedCardIndex } = this.state;

		if (!cards) return [];

		return Object.values(cards!).map((card, idx) => (
			<CardView
				key={card.cardNumber}
				onClick={() => this.handleCardChange(card, idx)}
				selected={idx === selectedCardIndex}
				borderWithErrorColor={cardValidationError}
				cardNumber={`${card.cardNumber}`}
				cardColor={card.cardColor}
				cardExpiration={card.cardExpiration}
			/>
		));
	};

	render() {
		const { cards } = this.props;

		return (
			<CardsSelectorS>
				{cards ? (
					<CardsSelectorListS>
						{this.renderCardsItem()}
					</CardsSelectorListS>
				) : (
					<NoCardTextS>
						Vous n'avez toujours pas de cartes enregistrées.
						<br />
						Rendez-vous dans l'onglet Cartes.
					</NoCardTextS>
				)}
			</CardsSelectorS>
		);
	}
}

 export default CardsSelector;
