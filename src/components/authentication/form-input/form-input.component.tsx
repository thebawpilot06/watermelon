//React
import React from "react";

import {
	GroupContainer,
	FormInputContainer,
	FormInputLabel,
} from "./form-input.styles";

export interface FormInputProps {
	handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
	label: string;
	type: string;
	value: string;
	displayWarningColor: boolean;
	displayValidColor: boolean;
}

const FormInput: React.FunctionComponent<FormInputProps> = ({
	handleChange,
	value,
	label,
	...props
}) => (
	<GroupContainer>
		<FormInputContainer
			onChange={handleChange}
            value={value}
            autoComplete="false"
			{...props}
		/>
		{label ? (
			<FormInputLabel
				className={value.length ? "shrink" : ""}
				displayWarningColor={props.displayWarningColor}
				displayValidColor={props.displayValidColor}
			>
				{label}
			</FormInputLabel>
		) : null}
	</GroupContainer>
);
export default FormInput;
