// Styled Components
import styled from "styled-components";

const SignInS = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: flex-start;
	width: 380px;

	& button {
		margin-top: 30px;
		width: 250px;
	}
`;

export default SignInS;
