/* eslint-disable no-unused-vars,react/no-did-update-set-state,implicit-arrow-linebreak */
// React
import React from "react";

/* Redux */
import { connect } from "react-redux";

// Actions
import {
	emailSignInStart,
	emailSignInType,
} from "../../../redux/user/user.actions";

// Selectors
import { selectSignInFailure } from "../../../redux/user/user.selectors";

/* Utils */
import { checkEmail, checkPassword } from "../../../utils/form/form.utils";

/* Lang */
import lang from "../../../ressources/lang/error/database-error.lang.json";

/* Components */

// Styles
import SignInS from "./sign-in.styles";

// Owns
import Notification, { NOTIFACTION_BG } from "../../generic/notification/notification.component";
import CustomButton from "../../generic/custom-button/custom-button.component";
import FormInput from "../../generic/form-input/form-input.component";

// Interface
import IRootState from "../../../redux/root/root.types";

import { ObjectContent } from "../../../ressources/interfaces/generic.interfaces";

interface SignInActionProps {
	emailSignInStart: emailSignInType;
}

interface SignInProps {
	signInFailure: string | null;
}

interface SignInState {
	email: ObjectContent;
	password: ObjectContent;
	submitting: boolean;
}

class SignIn extends React.Component<
	SignInActionProps & SignInProps,
	SignInState
> {
	constructor(props: any) {
		super(props);
		this.state = {
			email: {
				value: "",
				error: false,
			},
			password: {
				value: "",
				error: false,
			},
			submitting: false,
		};
	}

	componentDidUpdate(
		prevProps: SignInActionProps & SignInProps,
		prevState: SignInState,
	) {
		if (this.props.signInFailure && this.state.submitting) {
			let errMessage;

			this.setState({ submitting: false });

			// On va maintenant afficher un message d'erreur
			if (this.props.signInFailure === lang.userInexistant) {
				errMessage = "Vous n'avez pas de compte chez nous 🥺";
			} else if (this.props.signInFailure === lang.wrongPassword) {
				errMessage = "🔒 Mauvais mot de passe 🔒";
			}

			Notification.instance.show(
				NOTIFACTION_BG.WARNING,
				errMessage as string,
			);
		}
	}

	onEmailChange = (ev: React.SyntheticEvent<HTMLInputElement>) =>
		this.setState({
			email: {
				value: ev.currentTarget.value,
				error: !checkEmail(ev.currentTarget.value),
			},
		});

	onPasswordChange = (ev: React.SyntheticEvent<HTMLInputElement>) =>
		this.setState({
			password: {
				value: ev.currentTarget.value,
				error: !checkPassword(ev.currentTarget.value),
			},
		});

	onSubmit = () => {
		const { email, password } = this.state;

		// Si l'utilisateur ne rentre rien on n'a pas verifier les champs, il faut les verifier ici
		if (!email.value || !password.value) {
			this.setState((state) => ({
				...state,
				email: {
					...state.email,
					error: !checkEmail(state.email.value),
				},
				password: {
					...state.password,
					error: !checkPassword(state.password.value),
				},
			}));

			return;
		}

		// On verifie tout d'abord qu'il n' y a pas d'erreurs
		if (email.error || password.error) return;

		// Sinon on envoie la donnee a firebase pour verification et on fait patienter l'utilisateur
		this.setState({ submitting: true });

		this.props.emailSignInStart(email.value, password.value);
	};

	render() {
		const { email, password, submitting } = this.state;

		return (
			<SignInS>
				<h2>Connexion</h2>
				<FormInput
					handleChange={this.onEmailChange}
					type="text"
					label="Email"
					value={email.value}
					displayWarningColor={email.error}
					displayValidColor={!email.error && !!email.value}
				/>
				<FormInput
					handleChange={this.onPasswordChange}
					type="password"
					label="Mot de passe"
					value={password.value}
					displayWarningColor={password.error}
					displayValidColor={!password.error && !!password.value}
				/>
				<CustomButton onClick={this.onSubmit} loading={submitting}>
					Se connecter
				</CustomButton>
			</SignInS>
		);
	}
}

const mapStateToProps = (state: IRootState): SignInProps => (
	{ signInFailure: selectSignInFailure(state) }
	);

export default connect(
	mapStateToProps,
	{ emailSignInStart	},
)(SignIn);
