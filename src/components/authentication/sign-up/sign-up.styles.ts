import styled from "styled-components";

const SignUpS = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: flex-start;
	width: 380px;

	& input {
		z-index: -5;
	}

	& button {
		margin-top: 30px;
	}
`;

export default SignUpS;
