// React
import React from "react";

/* Redux */
import { connect } from "react-redux";

// Actions
import {
  signUpStart,
  signUpType,
} from "../../../redux/user/user.actions";

// Selectors
import { selectSignUpFailure } from "../../../redux/user/user.selectors";

/* Utils */
import {
  checkUserName,
  checkEmail,
  checkPassword,
  checkPasswordsMatch,
} from "../../../utils/form/form.utils";

/* Lang */
import lang from "../../../ressources/lang/error/database-error.lang.json";

/* Components */

// Styles
import SignUpS from "./sign-up.styles";

// Own
import Notification, { NOTIFACTION_BG } from "../../generic/notification/notification.component";
import CustomButton from "../../generic/custom-button/custom-button.component";
import FormInput from "../../generic/form-input/form-input.component";

// Interface
import IRootState from "../../../redux/root/root.types";

import { ObjectContent } from "../../../ressources/interfaces/generic.interfaces";

interface SignUpActionProps {
    signUpStart: signUpType,
}

interface SignUpProps {
    signUpFailure: string | null,
}

interface SignUpState {
    fullName: ObjectContent;
    email: ObjectContent;
    password: ObjectContent;
    passwordMatch: ObjectContent;
    submitting: boolean;
}

class SignUp extends React.Component<SignUpActionProps & SignUpProps, SignUpState> {
  constructor(props: any) {
    super(props);
    this.state = {
      fullName: {
        value: "",
        error: false,
      },
      email: {
        value: "",
        error: false,
      },
      password: {
        value: "",
        error: false,
      },
      passwordMatch: {
        value: "",
        error: false,
      },
      submitting: false,
    };
  }

    componentDidUpdate(prevProps: SignUpActionProps & SignUpProps, prevState: SignUpState) {
        if (this.props.signUpFailure && this.state.submitting) {
          // eslint-disable-next-line react/no-did-update-set-state
            this.setState({ submitting: false });

            // On va afficher un message d'erreur
            if (this.props.signUpFailure === lang.userAlreadyInDb) {
                Notification.instance.show(NOTIFACTION_BG.WARNING, "Vous avez déjà un compte chez nous 😙");
            }
        }
    }


    onFullNameChange = (ev: React.SyntheticEvent<HTMLInputElement>) => this.setState({
      fullName: {
        value: ev.currentTarget.value,
        error: !checkUserName(ev.currentTarget.value),
      },
    });


    onEmailChange = (ev: React.SyntheticEvent<HTMLInputElement>) => this.setState({
      email: {
        value: ev.currentTarget.value,
        error: !checkEmail(ev.currentTarget.value),
      },
    });

    onPasswordChange = (ev: React.SyntheticEvent<HTMLInputElement>) => {
      if (ev.currentTarget.name === "password") {
        this.setState({
          password: {
            value: ev.currentTarget.value,
            error: !checkPassword(ev.currentTarget.value),
          },
        });
      } else {
        this.setState({
          passwordMatch: {
            value: ev.currentTarget.value,
            error: !(checkPassword(ev.currentTarget.value) && checkPasswordsMatch(
              // eslint-disable-next-line react/no-access-state-in-setstate
              this.state.password.value,
              ev.currentTarget.value,
            )),
          },
        });
      }
    };

    onSubmit = () => {
      const { fullName,
              email,
              password,
              passwordMatch } = this.state;

      // Si l"utilisateur ne rentre rien on n'a pas verifier les champs, il faut les verifier ici
      if (!fullName.value || !email.value || !password.value || !passwordMatch.value) {
        this.setState((state) => ({
          fullName: {
            ...state.fullName,
            error: !checkUserName(state.fullName.value),
          },
          email: {
            ...state.email,
            error: !checkEmail(state.email.value),
          },
          password: {
            ...state.password,
            error: !checkPassword(state.password.value),
          },
          passwordMatch: {
            ...passwordMatch,
            error: !(checkPassword(state.passwordMatch.value) && checkPasswordsMatch(
              state.password.value,
              state.passwordMatch.value,
            )),
          },
        }));

        return;
      }

      // On verifie tout d'abord qu'il n' y a pas d'erreurs
      if (fullName.error || email.error || password.error || passwordMatch.error) return;

      // Sinon on peut essayer de s'inscrire

      // On affiche le loader
      this.setState({ submitting: true });

      this.props.signUpStart(fullName.value, email.value, password.value);
    };

    render() {
      const { fullName,
              email,
              password,
              passwordMatch,
              submitting } = this.state;

      return (
        <SignUpS>
          <h2>Inscription</h2>
          <FormInput
            handleChange={this.onFullNameChange}
            type="text"
            label="Nom Complet"
            value={fullName.value}
            helper={<p>Le nom complet doit être de la forme Prénom Nom</p>}
            displayWarningColor={fullName.error}
            displayValidColor={!fullName.error && !!fullName.value}
          />
          <FormInput
            handleChange={this.onEmailChange}
            type="text"
            label="Email"
            value={email.value}
            displayWarningColor={email.error}
            displayValidColor={!email.error && !!email.value}
          />
          <FormInput
            handleChange={this.onPasswordChange}
            type="password"
            name="password"
            label="Mot de passe"
            value={password.value}
            helper={(
              <p>
                - Entre 8 et 15 caractères
                <br />
                - Au moins une majuscule
                <br />
                - Au moins une miniscule
                <br />
                - Au moins un digit
                <br />
                - Au moins un des caractères suivants: -+!*$@%_
              </p>
            )}
            displayWarningColor={password.error}
            displayValidColor={!password.error && !!password.value}
          />
          <FormInput
            handleChange={this.onPasswordChange}
            type="password"
            name="passwordConfirm"
            label="Confirmation du mot de passe"
            value={passwordMatch.value}
            displayWarningColor={passwordMatch.error}
            displayValidColor={!passwordMatch.error && !!passwordMatch.value}
          />
          <CustomButton
            onClick={this.onSubmit}
            loading={submitting}
          >
            S'inscrire
          </CustomButton>
        </SignUpS>
      );
    }
}

const mapStateToProps = (state: IRootState): SignUpProps => (
  { signUpFailure: selectSignUpFailure(state) }
  );

export default connect(mapStateToProps, { signUpStart })(SignUp);
