import React from "react";

// Redux


/* Utils */

// Lodash
import { isEmpty } from "lodash";

/* Components */

// Styles
import { SelectorControllerS } from "./selector-controller.styles";

// Own
import SelectorInput from "../selector-input/selector-input.component";
import SelectorBody from "../selector-body/selector-body.component";


/* Interfaces */

interface SelectorControllerActionProps {
	onItemSelected(item: string): void;
}

interface SelectorControllerProps {
	items: string[];
	placeHolderText: string
}

interface SelectorControllerState {
	inputValue: string;
	inputHasFocus: boolean;
	placeHolderText: string;
	items: string[];
	itemSelected: string | null;
}

class SelectorController extends React.Component<
	SelectorControllerActionProps & SelectorControllerProps,
	SelectorControllerState
> {
	constructor(
		props: SelectorControllerActionProps & SelectorControllerProps,
	) {
		super(props);

		this.state = {
			inputValue: "",
			inputHasFocus: false,
			placeHolderText: "",
			items: [],
			itemSelected: null,
		};
	}

	static getDerivedStateFromProps(
		props: SelectorControllerActionProps & SelectorControllerProps,
		state: SelectorControllerState,
	) {
		if (props.placeHolderText.length > 0 && !state.placeHolderText.length) {
			return {
				...state,
				placeHolderText: props.placeHolderText,
			};
		}

		if (!isEmpty(props.items) && isEmpty(state.items)) {
			return {
				...state,
				items: props.items,
			};
		}

		return null;
	}

	handleInputValueChanged = (ev: React.SyntheticEvent<HTMLInputElement>) => {
		this.setState({ inputValue: ev.currentTarget.value });
	};

	handleCancelButtonClicked = () => {
		const { placeHolderText } = this.props;

		this.setState({
			inputValue: "",
			placeHolderText,
			itemSelected: null,
		});
	};

	handleFocusToggle = () => {
		this.setState((state) => ({ inputHasFocus: !state.inputHasFocus }));
	};

	handleItemSelected = (item: string) => {
		this.setState({
			inputValue: "",
			placeHolderText: item,
			itemSelected: item,
		});

		this.props.onItemSelected(item);
	};

	render() {
		const { inputValue, inputHasFocus, placeHolderText, items, itemSelected } = this.state;
		return (
			<SelectorControllerS>
				<SelectorInput
					onValueChange={this.handleInputValueChanged}
					onDeleteButtonClicked={this.handleCancelButtonClicked}
					onFocusChanged={this.handleFocusToggle}
					inputValue={inputValue}
					inputHasFocus={inputHasFocus}
					placeHolderText={placeHolderText}
					itemSelected={!!itemSelected}
				/>
				<SelectorBody
					onItemSelected={this.handleItemSelected}
					items={items}
					itemSelected={itemSelected}
					query={inputValue}
				/>
			</SelectorControllerS>
		);
	}
}

export default SelectorController;
