import styled from "styled-components";

// eslint-disable-next-line import/prefer-default-export
export const SelectorControllerS = styled.div`
    height: 320px;
    width: 450px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
`;
