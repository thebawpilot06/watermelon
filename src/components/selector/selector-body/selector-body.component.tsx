import React from "react";

/* Utils */

// Lodash
import { isEmpty } from "lodash";

/* Components */

// Styles
import {
	SelectorBodyS,
	SelectorBodyListControllerS,
	SelectorBodyListItem,
	NoMatchS,
} from "./selector-body.styles";

/* Interfaces */
interface SelectorBodyActionProps {
	onItemSelected(item: string): void;
}

interface SelectorBodyProps {
	items: string[];
	itemSelected: string | null;
	query: string;
}

const SelectorBody: React.FunctionComponent<
	SelectorBodyActionProps & SelectorBodyProps
> = ({ ...props }) => {
	const renderBodyItems = () => {
		const { items, query } = props;

		const filteredItems = items
			.filter((item) => item.includes(query))
			.map((item) => (
				<SelectorBodyListItem
					key={item}
					onClick={() => props.onItemSelected(item)}
					selected={item === props.itemSelected}
				>
					{item}
				</SelectorBodyListItem>
			));

		if (isEmpty(filteredItems)) {
			return (
				<NoMatchS>Aucun item ne correspond à votre requête</NoMatchS>
			);
		}

		return (
			<SelectorBodyListControllerS>
				{filteredItems}
			</SelectorBodyListControllerS>
		);
	};
	return (
		<SelectorBodyS queryLength={props.query.length}>
			{renderBodyItems()}
		</SelectorBodyS>
	);
};

export default SelectorBody;
