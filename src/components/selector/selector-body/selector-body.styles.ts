import styled from "styled-components";

// Colors
import { Colors } from "../../../ressources/styles/colors.styles";

const heightItem = "50px";

export const SelectorBodyS = styled.div<{queryLength: number}>`
    margin-top: 1rem;
    min-height: ${heightItem};
    max-height: 300px;
    width: 100%;
    background-color: "red";
    overflow: scroll;
    border: 1px solid ${Colors.lightGrey};
    border-radius: 10px;
    display: ${props => props.queryLength > 0 ? "flex" : "none"};
    flex-direction: column;
    justify-content: center;
    align-items: center;
    transition: all 0.2s ease-in;
`;

export const SelectorBodyListControllerS = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    cursor: pointer;
`;

export const SelectorBodyListItem = styled.div<{selected: boolean}>`
    height: ${heightItem};
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    background-color: ${props => props.selected ? Colors.blue : "white"};
    align-items: center;
    font-family: "Open Sans Condensed", sans-serif;
    font-size: 1.2rem;

    &:hover {
        background-color: ${Colors.lightBlue};
    }
`;

export const NoMatchS = styled.span`
    font-family: 'Open Sans Condensed';
    font-size: 1.2rem;
`;
