import React from "react";

/* Components */
// Pictures
import { ReactComponent as CancelPic } from "../../../ressources/assets/cancel-without-bg.svg";

// Styles
import {
	SelectorInputContainerS,
	SelectorInputS,
	CancelButtonS,
} from "./selector-input.styles";

/* Interfaces */

interface SelectorInputActionProps {
	onValueChange(ev: React.SyntheticEvent<HTMLInputElement>): void;
	onDeleteButtonClicked(): void;
	onFocusChanged(): void;
}

interface SelectorInputProps {
    inputValue: string;
	inputHasFocus: boolean;
	placeHolderText: string;
	itemSelected: boolean;
}

/*
interface SelectorInputState {
	inputHasFocus: boolean;
	inputValue: string;
}
 */

const SelectorInput: React.FunctionComponent<
	SelectorInputActionProps & SelectorInputProps
> = ({ ...props }) => (
	<SelectorInputContainerS inputHasFocus={props.inputHasFocus}>
		<SelectorInputS
			onChange={props.onValueChange}
			onFocus={props.onFocusChanged}
			onBlur={props.onFocusChanged}
			placeholder={props.placeHolderText}
			value={props.inputValue}
		/>
		<CancelButtonS
			onClick={props.onDeleteButtonClicked}
			inputHasFocus={props.inputHasFocus}
			itemSelected={props.itemSelected}
		>
			<CancelPic />
		</CancelButtonS>
	</SelectorInputContainerS>
);

export default SelectorInput;
