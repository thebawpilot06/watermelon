import styled from "styled-components";

// Colors
import { Colors } from "../../../ressources/styles/colors.styles";

const height = "50px";

export const SelectorInputContainerS = styled.div<{inputHasFocus: boolean}>`
    position: relative;
    height: ${height};
    width: 450px;
    padding-left: 1rem;
    background-color: white;
    border-radius: 10px;
    border: 1px solid ${props => props.inputHasFocus ? Colors.orange : Colors.lightGrey};
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    transition: all .2s ease-out;
`;

export const SelectorInputS = styled.input`
   height: 100%;
   width: 80%;
   border: none;
   outline: none;
   font-family: 'Open Sans Condensed', sans-serif;
   font-size: 1.2rem;
`;

export const CancelButtonS = styled.div<{inputHasFocus: boolean, itemSelected: boolean}>`
    position: absolute;
    right: 0;
    fill: ${Colors.lightBlack};
    height: ${height};
    width: ${height};
    cursor: pointer;
    display: ${props => props.itemSelected ? "flex" : "none"};
    flex-direction: column;
    justify-content: center;
    flex-direction: center;
    transition: all .2s ease-out;
    
    & svg {
        height: 30%;
        width: 30%;
        fill: ${props => props.inputHasFocus ? Colors.black : Colors.lightBlack};
    }
`;
