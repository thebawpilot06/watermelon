import React from "react";

/* Component */

// Own
import WatermelonSpinner from "./watermelon-spinner.component";

/* Interface */

interface IWithSpinner {
	isLoading: boolean;
}

const WithSpinner = <P extends object>(
	WrappedComponent: React.ComponentType<P>,
): React.FunctionComponent<IWithSpinner & P> => ({
	isLoading,
	...otherProps
}: IWithSpinner) => {
	return isLoading ? (
		<WatermelonSpinner />
	) : (
		<WrappedComponent {...(otherProps as P)} />
	);
};

export default WithSpinner;
