import React from "react";

/* Components */

// Logo
import { ReactComponent as WaterMelonLogo } from "../../ressources/assets/watermelon.svg";

// Styles
import WaterMelonSpinnerS from "./watermelon-spinner.styles";

const WaterMelonSpinner: React.FunctionComponent = () => (
	<WaterMelonSpinnerS>
		<WaterMelonLogo />
	</WaterMelonSpinnerS>
);

export default WaterMelonSpinner;
