import styled from "styled-components";

const WaterMelonSpinnerS = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    & svg {
        height: 20%;
        width:  20%;
        animation: rotation 2s infinite linear;
    }

    @keyframes rotation {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(359deg);
    }
}
`;

export default WaterMelonSpinnerS;
