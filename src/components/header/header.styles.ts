import styled from "styled-components";

/* React Router */
import { NavLink } from "react-router-dom";

/* Colors */
import { Colors } from "../../ressources/styles/colors.styles";

const avatarSize = 140;

export const headerWidth = 20;

export const HeaderS = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    height: 100vh;
    width: ${headerWidth}vw;
    background-color: ${Colors.darkGreen};
`;

export const HeaderContentS = styled.div`
    margin-top: 5rem;
    display:flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
`;

export const HeaderNavContentS = styled.div`
    margin-top: 8rem;
    height: 200px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
`;

export const NavLinkContainerS = styled.div`
    position: relative;
`;

export const NavLinkS = styled(NavLink)`
    color: white;
    font-family: "Roboto Slab", sans-serif;
    font-size: 1.5rem;
    font-weight: bold;
    text-transform: uppercase;

    letter-spacing: 0.4rem;

    &:before
    {
        position: absolute;
        width: 2px;
        height: 100%;
        left: 0px;
        top: 0px;
        content: "";
        background-color: #FFF;
        opacity: 0.2;
        transition: all 0.2s ease-out;
}

    &:hover:before, &.fullColored:before
    {
        width: 100%;
    }

`;

export const HeaderAvatarS = styled.img`
    height: ${avatarSize}px;
    width: ${avatarSize}px;
    border-radius: ${avatarSize}px;
`;

export const LogOutContainerS = styled.div`
    position: absolute;
    bottom: 1rem;
    left: 1.5rem;
    font-family: "Roboto Slab", sans-serif;
    font-size: 1.4rem;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    color: white;
    cursor: pointer;

    & svg {
        fill: white;
        height: 50px;
        width: 50px;
        margin-right: 1.6rem;
    }
`;
