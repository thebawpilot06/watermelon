import React from "react";

/* Redux */
import { connect } from "react-redux";

// Action
import {
    signOutStart,
    // eslint-disable-next-line no-unused-vars
    signOutType,
} from "../../redux/user/user.actions";

/* Images */
import DefaultImage from "../../ressources/assets/man.svg";

/*  Components */

// Logo
import { ReactComponent as LogOutLogo } from "../../ressources/assets/logout.svg";

// Styles
import {
    HeaderS,
    HeaderContentS,
    HeaderAvatarS,
    HeaderNavContentS,
    NavLinkContainerS,
    NavLinkS,
    LogOutContainerS,
} from "./header.styles";

/* Interfaces */
// eslint-disable-next-line no-unused-vars
import { User } from "../../redux/user/user.types";

interface HeaderProps {
    signOutStart: signOutType,
    user: User | null,
}

const Header: React.FunctionComponent<HeaderProps> = ({ user, ...props }) => {
    return (
        <HeaderS>
            <HeaderContentS>
                <HeaderAvatarS
                    src={user ? user.avatar : DefaultImage}
                />
                <HeaderNavContentS>
                    <NavLinkContainerS>
                        <NavLinkS
                            exact
                            to="/"
                            activeClassName="fullColored"
                        >
                            Accueil
                        </NavLinkS>
                    </NavLinkContainerS>
                    <NavLinkContainerS>
                        <NavLinkS
                            exact
                            to="/cards"
                            activeClassName="fullColored"
                        >
                            Mes cartes
                        </NavLinkS>
                    </NavLinkContainerS>
                    <NavLinkContainerS>
                        <NavLinkS
                            exact
                            to="/account"
                            activeClassName="fullColored"
                        >
                            Mon Compte
                        </NavLinkS>
                    </NavLinkContainerS>
                </HeaderNavContentS>
            </HeaderContentS>
            <LogOutContainerS
                onClick={props.signOutStart}
            >
                <LogOutLogo />
                Log Out
            </LogOutContainerS>
        </HeaderS>
    );
};

export default connect(null, { signOutStart })(Header);
