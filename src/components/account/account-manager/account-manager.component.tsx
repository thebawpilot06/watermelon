import React from "react";

/* Redux */
import { connect } from "react-redux";

// Selectors
import {
	selectCurrentUserFullName,
	selectCurrentUserEmail,
	selectCurrentUserPassword,
} from "../../../redux/user/user.selectors";

// Actions
import {
	updateUserDataRequest,
	updateUserDataRequestType,
} from "../../../redux/user/user.actions";

/* Utils */
// Colors
import { Colors } from "../../../ressources/styles/colors.styles";

// Form
import {
	checkUserName,
	checkEmail,
	checkPassword,
} from "../../../utils/form/form.utils";

/* Components */
// Pictures
import { ReactComponent as VisibleEye } from "../../../ressources/assets/password-view.svg";
import { ReactComponent as HiddenEye } from "../../../ressources/assets/password-hide.svg";

// Styles
import {
	AccountManagerS,
	AccountManagerInputsS,
	PasswordInputContainerS,
} from "./account-manager.styles";

// Own
import Notification, { NOTIFACTION_BG } from "../../generic/notification/notification.component";
import CustomButton from "../../generic/custom-button/custom-button.component";
import FormInput from "../../generic/form-input/form-input.component";

/* Interfaces */
import IRootState from "../../../redux/root/root.types";
import { ObjectContent } from "../../../ressources/interfaces/generic.interfaces";

interface AccountManagerActionProps {
	updateUserDataRequest: updateUserDataRequestType;
}

interface AccountManagerProps {
	fullName: string;
	email: string;
	password: string;
}

interface AccountManagerState {
	fullName: ObjectContent;
	email: ObjectContent;
	password: ObjectContent;
	displayPassword: boolean;
}

class AccountManager extends React.Component<
	AccountManagerActionProps & AccountManagerProps,
	AccountManagerState
> {
	constructor(props: AccountManagerActionProps & AccountManagerProps) {
		super(props);

		this.state = {
			fullName: {
				value: "",
				error: false,
			},
			email: {
				value: "",
				error: false,
			},
			password: {
				value: "",
				error: false,
			},
			displayPassword: false,
		};
	}

	static getDerivedStateFromProps(
		props: AccountManagerActionProps & AccountManagerProps,
		state: AccountManagerState,
	) {
		if (
			props.fullName &&
			!state.fullName.value &&
			props.email &&
			!state.email.value &&
			props.password &&
			!state.password.value
		) {
			return {
				...state,
				fullName: {
					...state.fullName,
					value: props.fullName,
				},
				email: {
					...state.email,
					value: props.email,
				},
				password: {
					...state.password,
					value: props.password,
				},
			};
		}

		return null;
	}

	handleFullNameChanged = (ev: React.SyntheticEvent<HTMLInputElement>) => {
		const inputValue = ev.currentTarget.value;

		this.setState({
			fullName: {
				value: inputValue,
				error: !checkUserName(inputValue),
			},
		});
	};

	handleEmailChanged = (ev: React.SyntheticEvent<HTMLInputElement>) => {
		ev.preventDefault();

		const inputValue = ev.currentTarget.value;

		this.setState({
			email: {
				value: inputValue,
				error: !checkEmail(inputValue),
			},
		});
	};

	handlePasswordChanged = (ev: React.SyntheticEvent<HTMLInputElement>) => {
		ev.preventDefault();

		const inputValue = ev.currentTarget.value;

		this.setState({
			password: {
				value: inputValue,
				error: !checkPassword(inputValue),
			},
		});
	};

	togglePasswordVisibility = () => {
		this.setState((prevState) => ({
			...prevState,
			displayPassword: !prevState.displayPassword,
		}));
	};

	handleOnModify = () => {
		const {
			fullName,
			email,
			password,
			} = this.state;

		// On verifie tout d'abord qu'il n' y a pas d'erreurs
		if (fullName.error || email.error || password.error) return;

		// On re initialise le state pour que la fonction getDeriveStateFromProps marche

		this.props.updateUserDataRequest(fullName.value, email.value, password.value);

		// On informe l'utilisateur de la modification
		Notification.instance.show(NOTIFACTION_BG.SUCCESS, "Compte modifié 👍🏻");
	};

	render() {
		const { fullName, email, password, displayPassword } = this.state;
		return (
			<AccountManagerS>
				<AccountManagerInputsS autoComplete="test">
					<FormInput
						handleChange={this.handleFullNameChanged}
						type="text"
						label="Nom Complet"
						value={fullName.value}
						displayWarningColor={fullName.error}
						displayValidColor={!fullName.error && !!fullName.value}
					/>
					<FormInput
						handleChange={this.handleEmailChanged}
						type="text"
						label="Email"
						value={email.value}
						displayWarningColor={email.error}
						displayValidColor={!email.error && !!email.value}
					/>
					<PasswordInputContainerS>
						<FormInput
							handleChange={this.handlePasswordChanged}
							type={displayPassword ? "text" : "password"}
							name="password"
							label="Mot de Passe"
							value={password!.value as string}
							helper={
								(
									<p>
										- Entre 8 et 15 caractères
										<br />
										- Au moins une majuscule
										<br />
										- Au moins une miniscule
										<br />
										- Au moins un digit
										<br />
										- Au moins un des caractères suivants: -+!*$@%_
									</p>
								)
							}
							displayWarningColor={password.error}
							displayValidColor={
								!password.error && !!password.value
							}
						/>
						{displayPassword ? (
							<HiddenEye
								onClick={this.togglePasswordVisibility}
							/>
						) : (
							<VisibleEye
								onClick={this.togglePasswordVisibility}
							/>
						)}
					</PasswordInputContainerS>
				</AccountManagerInputsS>
				<CustomButton
					onClick={this.handleOnModify}
					color={Colors.orange}
				>
					Modifier
				</CustomButton>
			</AccountManagerS>
		);
	}
}

const mapStateToProps = (state: IRootState): AccountManagerProps => ({
	fullName: selectCurrentUserFullName(state),
	email: selectCurrentUserEmail(state),
	password: selectCurrentUserPassword(state),
});

export default connect(
	mapStateToProps,
	{
		updateUserDataRequest,
	},
)(AccountManager);
