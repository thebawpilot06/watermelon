import styled from "styled-components";

export const AccountManagerS = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-content: center;
`;

export const AccountManagerInputsS = styled.form`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-content: center;
`;

export const PasswordInputContainerS = styled.div`
    position: relative;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;

    & svg {
        position: absolute;
        right: 1rem;
        top: 2.3rem;
        cursor: pointer;
        height: 20px;
        width: 20px;
    }
`;