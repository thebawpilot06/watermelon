import React from "react";

/* Components */

// ReactCss
import ReactCssTransitionGroup from "react-addons-css-transition-group";

// Styles
import { NotificationS } from "./notification.styles";

/* Interfaces */
export enum NOTIFACTION_BG {
    // eslint-disable-next-line no-unused-vars
    WARNING,
    // eslint-disable-next-line no-unused-vars
    INFO,
    // eslint-disable-next-line no-unused-vars
    SUCCESS,
}

/*
interface NotificationProps {

    notificationBg: NOTIFACTION_BG,
}
*/


interface NotificationState {
    show: boolean;
    notificationColor: NOTIFACTION_BG | null;
    message: string | null
}

class Notification extends React.Component<{}, NotificationState> {
    static instance: Notification;

    constructor(props: any) {
        super(props);

        this.state = {
            show: false,
            notificationColor: null,
            message: null,
        };

        Notification.instance = this;
    }

    private hide() {
        this.setState({ show: false });
    }

    show(notificationColor: NOTIFACTION_BG, message: string) {
        this.setState({
            show: true,
            notificationColor,
            message,
        });

        setTimeout(() => this.hide(), 2000);
    }

    render() {
        const { show, message, notificationColor } = this.state;

        return (
            <ReactCssTransitionGroup
                transitionName="notification"
                transitionEnter
                transitionLeave
                transitionEnterTimeout={500}
                transitionLeaveTimeout={300}
            >
                {
                    show ?
                      (
                        <NotificationS
                            key={message!}
                            notificationColor={notificationColor!}
                        >
                            {message}
                        </NotificationS>
                      )
                        : null
                }
            </ReactCssTransitionGroup>
        );
    }
}

export default Notification;
