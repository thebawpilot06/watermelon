import styled from "styled-components";

/* Colors */
import { Colors } from "../../../ressources/styles/colors.styles";

/* Enum */
import { NOTIFACTION_BG } from "./notification.component";

const getNotificationBackgroundColor = (color: NOTIFACTION_BG) => {
    switch (color) {
        case NOTIFACTION_BG.WARNING:
            return Colors.red;
        case NOTIFACTION_BG.INFO:
            return Colors.orange;
        case NOTIFACTION_BG.SUCCESS:
            return Colors.green;
        default:
            return Colors.grey;
    }
};

// eslint-disable-next-line import/prefer-default-export
export const NotificationS = styled.div<{notificationColor: NOTIFACTION_BG | null}>`
    font-family: "Roboto Slab", sans-serif;
    color: white;
    text-align: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 15px;
    background-color: ${props => getNotificationBackgroundColor(props.notificationColor!)};
    border-radius: 5px;
    position: absolute;
    top: 50px;
    left: 50%;
    transform: translateX(-50%);

    &.notification-enter {
        opacity: 0;
        transform: translate(-50%, -120px)
    }

    &.notification-enter.notification-enter-active {
        opacity: 1;
        transform: translate(-50%, 0px);
        transition: all .5s ease-out;
    }

    &.notification-leave {
        opacity: 1;
        transform: translateX(-50%);
    }

    &.notification-leave.notification-leave-active {
        opacity: 0;
        transform: translateX(-50%);
        transition: opacity .3s ease-in;
    }
`;
