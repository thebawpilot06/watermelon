// React
import React from "react";

// Components
import {
  GroupContainer,
  FormInputContainer,
  FormInputLabel,
  SmallS,
} from "./form-input.styles";

// Interfaces
interface FormInputProps {
	handleChange: (ev: React.SyntheticEvent<HTMLInputElement>) => void,
	label: string;
	type: string;
	name?: string;
	value: string;
	helper?: JSX.Element;
	displayWarningColor: boolean;
	displayValidColor: boolean;
}

const FormInput: React.FunctionComponent<FormInputProps> = (
  { handleChange, label, type, value, ...props },
  ) => (
  <GroupContainer>
    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
    <FormInputContainer onChange={handleChange} type={type} value={value} {...props} />
    {label ? (
      // eslint-disable-next-line react/jsx-props-no-spreading
      <FormInputLabel className={value.length ? "shrink" : ""} {...props}>
        {label}
      </FormInputLabel>
    ) : null}
    <SmallS>
      {props.helper ? props.helper : null}
    </SmallS>
  </GroupContainer>
);

export default FormInput;
