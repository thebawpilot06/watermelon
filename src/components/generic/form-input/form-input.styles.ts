// Styled Components
import styled, { css } from "styled-components";

// Colors
import { Colors } from "../../../ressources/styles/colors.styles";

/**
 * * Fonction permettant de styliser notre input. Dans le cas d'une erreur le label
 * et la bordure seront affiches
 * en rouge. Sinon si l'input verifie les conditions imposees par le dev, le label
 * et la bordure seront affiches
 * en vert. Le gris est la couleur initiale de l'input.
 *
 * @param displayWarningColor booleen indiquant si l'input doit etre affiche en rouge
 * @param displayValidColor   booleen indiquant si l'input doit etre affiche en vert
 */
const getFormInputColor = ({ displayWarningColor, displayValidColor }: {
  displayWarningColor: boolean;
  displayValidColor: boolean;
}) => {
  if (displayValidColor) return Colors.green;

  if (displayWarningColor) return Colors.red;

  return Colors.grey;
};

const shrinkLabelStyles = css`
	top: -17px;
	font-size: 12px;
	color: ${getFormInputColor};
`;

export const GroupContainer = styled.div`
	position: relative;
	margin: 25px 0;
	input[type="password"] {
		letter-spacing: 0.3em;
	}
`;

export const FormInputContainer = styled.input`
	background: none;
	color: ${getFormInputColor};
	font-size: 18px;
	padding: 10px 10px 10px 5px;
	display: block;
	width: 300px;
	border: none;
	border-radius: 0;
	border-bottom: 1px solid ${getFormInputColor};
	&:focus {
		outline: none;
	}
	&:focus ~ label {
		${shrinkLabelStyles};
	}
`;

export const FormInputLabel = styled.label`
	color: ${getFormInputColor};
	font-size: 14px;
	font-weight: normal;
	position: absolute;
	pointer-events: none;
	left: 5px;
	top: 10px;
	transition: 300ms ease all;

	&.shrink {
		${shrinkLabelStyles};
	}
`;

export const SmallS = styled.div`
	position: relative;
	top: 0.4rem;
	font-size: 0.6rem;
`;
