/* eslint-disable arrow-parens */
import styled from "styled-components";

/* Colors */
import { Colors } from "../../../ressources/styles/colors.styles";

const getDropZoneColor = (wrongType: boolean, highlight: boolean) => {
  if (wrongType) return Colors.red;
  if (highlight) return Colors.blue;
  return Colors.grey;
};

// eslint-disable-next-line import/prefer-default-export
export const DropZoneS = styled.div<{wrongType: boolean, highlight: boolean}>`
    height: 500px;
    width: 350px;
    padding: 0 20px;
    border: 2px dashed ${props => getDropZoneColor(props.wrongType, props.highlight)};
    border-radius: 10%;
    margin-left: 10px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    & svg {
        height: 120px;
        width: 120px;
    }

    & p {
        font-family: "Open Sans Condensed", sans-serif;
        text-align: center;
        color: ${props => getDropZoneColor(props.wrongType, props.highlight)}
    }
`;
