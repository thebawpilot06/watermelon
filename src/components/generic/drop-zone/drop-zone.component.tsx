import React from "react";

/* Utils */

// ClassNames
import classNames from "classnames";

/* Components */

// Styles
import { DropZoneS } from "./drop-zone.styles";

// Interface

interface IDropZoneProps {
	onFileUploaded: (file: File) => void;
	formatsAccepted: string[];
}

interface IDropZoneState {
	highlight: boolean;
	wrongType: boolean;
}

class DropZone extends React.Component<IDropZoneProps, IDropZoneState> {
	constructor(props: any) {
		super(props);

		this.state = {
			highlight: false,
			wrongType: false,
		};
	}

	validFileType = (type: string) => this.props.formatsAccepted.includes(type);

	checkFile = (ev: React.DragEvent<HTMLDivElement>) => {
		const { highlight, wrongType } = this.state;
		if (ev.dataTransfer.items && ev.dataTransfer.items[0]) {
			if (!this.validFileType(ev.dataTransfer.items[0].type)) {
				if (!wrongType) {
					this.setState({ wrongType: true	});
				}
			} else {
				if (!highlight) {
					this.setState({ highlight: true });
				}
			}
		} else if (ev.dataTransfer.files && ev.dataTransfer.files[0]) {
			if (!this.validFileType(ev.dataTransfer.files[0].type)) {
				if (!wrongType) {
					this.setState({	wrongType: true	});
				}
			} else {
				if (!highlight) {
					this.setState({	highlight: true	});
				}
			}
		}
	};

	onDragEnter = (ev: React.DragEvent<HTMLDivElement>) => {
		ev.preventDefault();

		// On verifie qu "on a bien un fichier de type image
		this.checkFile(ev);
	};

	onDragLeave = (ev: React.SyntheticEvent) => {
		ev.preventDefault();

		this.setState({
			highlight: false,
			wrongType: false,
		});
	};

	onDragOver = (ev: React.DragEvent<HTMLDivElement>) => {
		ev.stopPropagation();
		ev.preventDefault();

		this.checkFile(ev);
	};

	onDrop = (ev: React.DragEvent<HTMLDivElement>) => {
		ev.stopPropagation();
		ev.preventDefault();

		const { wrongType } = this.state;

		if (wrongType) return;

		// On fait loader
		this.setState({
			highlight: false,
			wrongType: false,
		});

		if (ev.dataTransfer.items && ev.dataTransfer.items[0]) {
			this.props.onFileUploaded(ev.dataTransfer.items[0].getAsFile()!);
		} else if (ev.dataTransfer.files && ev.dataTransfer.files[0]) {
			this.props.onFileUploaded(ev.dataTransfer.files[0]);
		}
	};

	render() {
		const { highlight, wrongType } = this.state;

		const dropZoneClassNames = classNames({ highlight });

		return (
			<DropZoneS
				className={dropZoneClassNames}
				draggable
				onDragEnter={this.onDragEnter}
				onDragLeave={this.onDragLeave}
				onDragOver={this.onDragOver}
				onDrop={this.onDrop}
				highlight={highlight}
				wrongType={wrongType}
			>
				{this.props.children}
			</DropZoneS>
		);
	}
}

export default DropZone;
