/* eslint-disable no-unneeded-ternary */
// Styled Components
import styled, { css } from "styled-components";

// Color
import { Colors } from "../../../ressources/styles/colors.styles";

const getCustomButtonColor = ({ hoverable, color }: { hoverable?: boolean; color?: string }) => {
  if (hoverable) {
    return css`
			color: white;
			background-color: ${color ? color : Colors.black};

			&:hover {
				color: ${color ? color : Colors.black};
				background-color: white;
				border: 1px solid ${color ? color : Colors.black};
			}
		`;
    // eslint-disable-next-line no-else-return
  } else {
    return css`
			color: white;
			background-color: ${color ? color : Colors.black};
		`;
  }
};

// eslint-disable-next-line import/prefer-default-export
export const CustomButtonS = styled.button`
	min-width: 180px;
	height: 50px;
	letter-spacing: 0.3rem;
	line-height: 50px;
	padding: 0 35px 0 35px;
	font-size: 15px;
	text-transform: uppercase;
	font-size: 0.8rem;
	outline: none;
	cursor: pointer;
	display: flex;
	justify-content: center;

	${getCustomButtonColor};
`;
