// React
import React from "react";

// Components
import { CustomButtonS } from "./custom-button.styles";
import { ReactComponent as SpinnerButton } from "../../../ressources/assets/spinnerButton.svg";

// Interfaces
interface CustomButtonProps {
    onClick: () => void;
    hoverable?: boolean; // Animation css
    loading?: boolean; // Si true remplace tout contenu par un spinner
    color?: string; // Coleur ecrite en valeur hexadecimale
}

const CustomButton: React.FunctionComponent<CustomButtonProps> = (props) => (
  <CustomButtonS
    onClick={props.onClick}
    hoverable={props.hoverable}
    color={props.color}
  >
    {
            !props.loading ?
              props.children
              : (
                <SpinnerButton
                  height="50px"
                  width="50px"
                />
              )
    }
  </CustomButtonS>
);

export default CustomButton;
