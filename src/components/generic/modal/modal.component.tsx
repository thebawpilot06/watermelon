import React from "react";

/* Utils */

// Classname
import className from "classnames";

/* Components */

// Logo
import { ReactComponent as CancelLogo } from "../../../ressources/assets/cancel.svg";

// Styles
import {
  ModalS,
  ModalContentS,
  HeaderS,
  BodyS,
  FooterS,
} from "./modal.styles";

// Interfaces ~  Enums
export enum ContentPosition {
  // eslint-disable-next-line no-unused-vars
  LEFT,
  // eslint-disable-next-line no-unused-vars
  CENTER,
  // eslint-disable-next-line no-unused-vars
  RIGHT
}

interface IModalState {
    hidden: boolean;
    hideCloseButton: boolean;
    headerContent?: JSX.Element;
    headerContentPos?: ContentPosition;
    headerTextColor?: string;
    bodyContent?: JSX.Element;
    bodyTextColor?: string;
    footerContent?: JSX.Element;
    footerContentPos?: ContentPosition;
    footerTextColor?: string;
}

class Modal extends React.Component<{}, IModalState> {
    static instance: Modal;

    constructor(props: any) {
      super(props);

      Modal.instance = this;

      this.state = {
        hidden: true,
        headerContent: undefined,
        headerContentPos: undefined,
        // eslint-disable-next-line react/no-unused-state
        headerTextColor: undefined,
        bodyContent: undefined,
        footerContent: undefined,
        footerContentPos: undefined,
        // eslint-disable-next-line react/no-unused-state
        footerTextColor: undefined,
        hideCloseButton: false,
      };
    }

    show = () => {
      this.setState({ hidden: false });
    };

    hide = () => {
      this.setState({
        hidden: true,
        headerContent: undefined,
        headerContentPos: undefined,
        // eslint-disable-next-line react/no-unused-state
        headerTextColor: undefined,
        bodyContent: undefined,
        // eslint-disable-next-line react/no-unused-state
        bodyTextColor: undefined,
        footerContent: undefined,
        footerContentPos: undefined,
        // eslint-disable-next-line react/no-unused-state
        footerTextColor: undefined,
      });
    };

    configure = (
      { headerContent,
        headerContentPos,
        headerTextColor,
       bodyContent,
       bodyTextColor,
        footerContent,
        footerContentPos,
        footerTextColor,
        hideCloseButton,
      }: {
        headerContent?: JSX.Element,
        headerContentPos?: ContentPosition,
        headerTextColor?: string,
        bodyContent: JSX.Element,
        bodyTextColor?: string,
        footerContent?: JSX.Element,
        footerContentPos?: ContentPosition,
        footerTextColor?: string,
        hideCloseButton: boolean,
      },
    ) => {
      this.setState((state) => ({
        headerContent,
        headerContentPos,
        // eslint-disable-next-line react/no-unused-state
        headerTextColor,
        bodyContent,
        // eslint-disable-next-line react/no-unused-state
        bodyTextColor,
        footerContent,
        footerContentPos,
        // eslint-disable-next-line react/no-unused-state
        footerTextColor,
        hideCloseButton,
      }));
    };

    render() {
      const { hidden,
              headerContent,
              headerContentPos,
              bodyContent,
              footerContent,
              footerContentPos } = this.state;

      const modalClassName = className({ "visible": !hidden });
      return (
        <ModalS className={modalClassName}>
          <ModalContentS>
            {
              !this.state.hideCloseButton ?
                <CancelLogo onClick={this.hide} className="cancel-logo" />
                : null
            }
            <HeaderS position={headerContentPos}>{headerContent}</HeaderS>
            <BodyS position={ContentPosition.CENTER}>{bodyContent}</BodyS>
            <FooterS position={footerContentPos}>{footerContent}</FooterS>
          </ModalContentS>
        </ModalS>
      );
    }
}

export default Modal;
