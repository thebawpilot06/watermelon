/* eslint-disable arrow-parens,no-confusing-arrow */
import styled, { css } from "styled-components";

// Enum
import { ContentPosition } from "./modal.component";

const getContentPosition = (position?: ContentPosition) => {
  switch (position) {
    case ContentPosition.LEFT:
      return "flex-start";
    case ContentPosition.CENTER:
      return "center";
    case ContentPosition.RIGHT:
      return "flex-end";
    default:
      return "center";
  }
};

const HeaderBodyFooterStyle = css<{position?: ContentPosition}>`
    display: flex;
    justify-content: ${props => getContentPosition(props.position)};
`;

export const ModalS = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    height: 100vh;
    width: 100vw;
    background-color: rgba(0,0,0,0.7);
    display: none;

    &.visible {
        display: block;
    }
`;

export const ModalContentS = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    min-height: 200px;
    min-width: 600px;
    background-color: white;
    border-radius: 20px;
    padding: 40px 30px; 
    color: red;
    z-index: 5;


    & .cancel-logo {
            height: 30px;
            width: 30px;
            cursor: pointer;
            position: absolute;
            right: 10px;
            top: 10px;
        }
`;


export const HeaderS = styled.div<{position?: ContentPosition, colorChosen?: string}>`
    color: ${props => props.colorChosen ? props.colorChosen : "black"};
    ${HeaderBodyFooterStyle}
`;

export const BodyS = styled.div<{position?: ContentPosition, colorChosen?: string}>`
    color: ${props => props.colorChosen ? props.colorChosen : "black"}
    ${HeaderBodyFooterStyle}
`;

export const FooterS = styled.div<{position?: ContentPosition, colorChosen?: string}>`
    margin-top: 3rem;
    color: ${props => props.colorChosen ? props.colorChosen : "black"};
    ${HeaderBodyFooterStyle}
`;
