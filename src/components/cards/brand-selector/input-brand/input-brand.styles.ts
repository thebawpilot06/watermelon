import styled from "styled-components";

const InputBrandS = styled.span`
  margin-right : 1rem;
  font-family: 'Roboto Slab', sans-serif;
  font-size: 0.8rem;

  & input {
    margin-right: 8px;
  }
`;

export default InputBrandS;
