import React from "react";

/* Components */
// styles
import InputBrandS from "./input-brand.styles";

/* Interface */

interface InputBrandProps {
  label : string;
  value: string;
  selected: boolean;
}

interface InputBrandActionProps {
  onButtonSelected : Function;
}

class InputBrand extends React.Component<InputBrandProps & InputBrandActionProps> {
  // eslint-disable-next-line no-useless-constructor
  constructor(props : InputBrandProps & InputBrandActionProps) {
    super(props);

  }

  onChangeValue = (ev: React.SyntheticEvent<HTMLInputElement>) => {
    this.props.onButtonSelected(ev.currentTarget.value);
  };

  render() {
    const { value, label, selected } = this.props;

    return (
      <InputBrandS>
        <input
          type="radio"
          name="brand"
          checked={selected}
          value={value}
          onChange={this.onChangeValue}
        />
        {label}
      </InputBrandS>
    );
  }
}

export default InputBrand;
