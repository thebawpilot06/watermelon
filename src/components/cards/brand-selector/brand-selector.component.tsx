import React from "react";

/* Component */
// styles
import {
	BrandSelectorS,
	BrandSelectorBodyS,
	BrandSelectorHeaderS,
} from "./brand-selector.styles";
import InputBrand from "./input-brand/input-brand.component";

/* Interface */
import { BrandName } from "../../../database/Cards.database";

interface BrandSelectorActionProps {
	onChange: Function;
}

interface BrandSelectorProps {
	error: boolean;
	selectedBrandName: string | null;
}

interface BrandSelectorState {
	brandNameSelected: string;
}

// eslint-disable-next-line max-len
class BrandSelector extends React.Component<
	BrandSelectorActionProps & BrandSelectorProps,
	BrandSelectorState
> {
	// eslint-disable-next-line no-useless-constructor
	constructor(props: BrandSelectorActionProps & BrandSelectorProps) {
		super(props);

		this.state = {
			brandNameSelected: "",
		};
	}

	static getDerivedStateFromProps(
		props: BrandSelectorActionProps & BrandSelectorProps,
		state: BrandSelectorState
	) {
		if (props.selectedBrandName && !state.brandNameSelected) {
			return {
				brandNameSelected: props.selectedBrandName,
			};
		}

		return null;
	}

	// fonction qui va remettre en forme la valeur de l'input puis appelé la
	// fonction passé en props pour retourner la valeur
	onButtonSelected = (value: string) => {
		this.setState({ brandNameSelected: value });

		this.props.onChange(value);
	};

	render() {
		const { brandNameSelected } = this.state;
		return (
			<BrandSelectorS>
				<BrandSelectorHeaderS>Brand :</BrandSelectorHeaderS>
				<BrandSelectorBodyS error={this.props.error}>
					<InputBrand
						label="Master Card"
						value={BrandName.MasterCard}
						selected={BrandName.MasterCard === brandNameSelected}
						onButtonSelected={this.onButtonSelected}
					/>
					<InputBrand
						label="Visa"
						value={BrandName.Visa}
						selected={BrandName.Visa === brandNameSelected}
						onButtonSelected={this.onButtonSelected}
					/>
					<InputBrand
						label="JCB"
						value={BrandName.JCB}
						selected={BrandName.JCB === brandNameSelected}
						onButtonSelected={this.onButtonSelected}
					/>
					<InputBrand
						label="American Express"
						value={BrandName.AmericanExpress}
						selected={
							BrandName.AmericanExpress === brandNameSelected
						}
						onButtonSelected={this.onButtonSelected}
					/>
					<InputBrand
						label="Union Pay"
						value={BrandName.UnionPay}
						selected={BrandName.UnionPay === brandNameSelected}
						onButtonSelected={this.onButtonSelected}
					/>
				</BrandSelectorBodyS>
			</BrandSelectorS>
		);
	}
}

export default BrandSelector;
