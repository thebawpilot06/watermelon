import styled from "styled-components";

/* Colors */
import { Colors } from "../../../ressources/styles/colors.styles";

const BrandSelectorS = styled.form`
	width: 100%;
	display: flex;
	flex-direction: row;
	justify-content: flex-start;
	align-items: center;
`;

const BrandSelectorBodyS = styled.div<{error: boolean}>`
	color: ${props => !props.error ? Colors.grey : Colors.red};
	display: flex;
	flex-direction: row;
	justify-content: flex-start;
	align-items: center;
`;

const BrandSelectorHeaderS = styled.span`
	font-size: 1.0rem;
	font-family: 'Open Sans Condensed', sans-serif;
	margin-right: 1rem;
`;

export {
  BrandSelectorS,
  BrandSelectorBodyS,
  BrandSelectorHeaderS,
};
