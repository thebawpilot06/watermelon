import styled from "styled-components";

/* Colors */
import { Colors } from "../../../ressources/styles/colors.styles";

export const CardFormS = styled.div`
	height: 100%;
	width: 100%;
`;

export const CardFormContentS = styled.div`
	height: 300px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: flex-end;
`;

export const CardFormInputsS = styled.div`
	height: 200px;
	width: 100%;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: flex-start;
`;

export const EditButtonsContainer = styled.div`
	width: 400px;
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
`;

export const CardNumberInputS = styled.input<{error: boolean}>`
	height: 40px;
	width: 160px;
	font-size: 0.9rem;
	font-family: 'Open Sans Condensed', sans-serif;
	border-style: solid;
	border-width: 1px;
	border-radius: 5px;
	padding-left: 0.6rem;
	outline: none;
	border-color: ${props => !props.error ? "transparent" : Colors.red};
	color: ${props => !props.error ? Colors.grey : Colors.red};
	-webkit-box-shadow: 4px 6px 15px -5px rgba(204,204,204,1);
	-moz-box-shadow: 4px 6px 15px -5px rgba(204,204,204,1);
	box-shadow: 4px 6px 15px -5px rgba(204,204,204,1);
`;

export const CardExpirationInputS = styled(CardNumberInputS)`
	width: 90px;
`;
