import React from "react";

/* Redux */
import { connect } from "react-redux";

// Actions
import {
	addCardRequest,
	editCardRequest,
	deleteCard,
	addCardRequestType,
	editCardRequestType,
	deleteCardType,
} from "../../../redux/cards/cards.actions";

/* Utils */

// Form
import { containsOnlyDigit } from "../../../utils/form/form.utils";

// Cards
import {
	checkCardNumberLength,
	checkCardExpiration,
	generateCardColor,
	getCardNumberFormatted,
} from "../../../utils/cards/cards.utils";

// Date
import { convertMMYYToDate } from "../../../utils/date/date.utils";

// Colors
import { Colors } from "../../../ressources/styles/colors.styles";

/* Components */

// Styles
import {
	CardFormS,
	CardFormContentS,
	CardFormInputsS,
	EditButtonsContainer,
	CardNumberInputS,
	CardExpirationInputS,
} from "./card-form.styles";

// Own
import Modal from "../../generic/modal/modal.component";
import CustomButton from "../../generic/custom-button/custom-button.component";
import BrandSelector from "../brand-selector/brand-selector.component";

/* Interfaces */
import { BrandName } from "../../../database/Cards.database";
import { ObjectContent, CardData } from "../../../ressources/interfaces/generic.interfaces";
import { Card, CardInput } from "../../../redux/cards/cards.types";

interface CardFormActionProps {
	addCardRequest: addCardRequestType;
	editCardRequest: editCardRequestType;
	deleteCard: deleteCardType;
}

interface CardFormProps {
	card?: CardData;
	editMode?: true;
}

interface CardFormState {
	number: ObjectContent;
	brand: ObjectContent;
	expiration: ObjectContent;
}

class CardForm extends React.Component<
	CardFormActionProps & CardFormProps,
	CardFormState
> {
	constructor(props: CardFormActionProps & CardFormProps) {
		super(props);

		this.state = {
			number: {
				value: "",
				error: false,
			},
			brand: {
				value: "",
				error: false,
			},
			expiration: {
				value: "",
				error: false,
			},
		};
	}

	static getDerivedStateFromProps(
		props: CardFormProps,
		state: CardFormState,
	): CardFormState | null {
		if (
			props.card &&
			!state.number.value &&
			!state.brand.value &&
			!state.expiration.value
		) {
			return {
				...state,
				number: {
					...state.number,
					value: props.card!.cardNumber,
				},
				brand: {
					...state.brand,
					value: props.card!.cardBrand,
				},
				expiration: {
					...state.expiration,
					value: props.card!.cardExpiration,
				},
			};
		}

		return null;
	}

	handleCardNumberChange = (ev: React.SyntheticEvent<HTMLInputElement>) => {
		const { number } = this.state;
		const inputValue = ev.currentTarget.value;

		// On verifie que le card number n'est pas en mode erreur (gestion de l'input)
		if (number.error && inputValue.length < number.value.length) {
			this.setState((state) => ({
				number: {
					...state.number,
					value: inputValue,
					error: inputValue.length === 0,
				},
			}));

			return;
		}
		if (inputValue.length < number.value.length) {
			this.setState((state) => ({
				number: {
					...state.number,
					value: inputValue,
				},
			}));

			return;
		}

		// On verifie ensuite d'abord que l'input ne contient que des digits
		if (!containsOnlyDigit(inputValue, "  ")) return;

		// On verifie tout d'abord que l'input est inferieur à 19 caracteres
		if (inputValue.length <= 4) {
			// Maintenant on doit verifier si on est en fin de partie des digits
			this.setState((state) => ({
				number: {
					...state.number,
					value: inputValue,
				},
			}));
		}
	};

	handleCardBrandChange = (brandName: string) => {
		this.setState((state) => ({
			brand: {
				...state.brand,
				value: brandName,
				error: false,
			},
		}));
	};

	handleCardExpirationChange = (
		ev: React.SyntheticEvent<HTMLInputElement>,
	) => {
		const { expiration } = this.state;
		const inputValue = ev.currentTarget.value;

		// On verifie que la date d'expiration n'est pas en mode erreur (gestion de l'input)
		if (expiration.error && inputValue.length < expiration.value.length) {
			this.setState((state) => ({
				expiration: {
					value: inputValue,
					error: inputValue.length === 0,
				},
			}));

			return;
		}

		if (inputValue.length < expiration.value.length) {
			this.setState((state) => ({
				expiration: {
					...state.expiration,
					value: inputValue,
				},
			}));

			return;
		}

		// On verifie ensuite que l'input ne contient que des digits
		if (!containsOnlyDigit(inputValue, " / ")) return;

		if (inputValue.length <= 7) {
			const inputWithoutSpace = inputValue.replace(/  /g, "");

			const endMonthPart = inputWithoutSpace.length === 2;

			this.setState((state) => ({
				expiration: {
					...state.expiration,
					value: endMonthPart ? `${inputValue} / ` : inputValue,
				},
			}));
		}
	};

	onValidate = () => {
		const { number, brand, expiration } = this.state;

		const { card, editMode } = this.props;

		// Maintenant nous devons verifier qu'il s'agit bien d'une carte valide
		let error = false;

		// Date
		if (!checkCardExpiration(expiration.value)) {
			error = true;

			this.setState((state) => ({
				...state,
				expiration: {
					...state.expiration,
					error: true,
				},
			}));
		}

		// Test du choix des brands
		if (brand.value === "") {
			error = true;

			this.setState((state) => ({
				...state,
				brand: {
					...state.brand,
					error: true,
				},
			}));
		}

		// Card
		if (!checkCardNumberLength(number.value)) {
			error = true;

			this.setState((state) => ({
				...state,
				number: {
					...state.number,
					error: true,
				},
			}));
		}

		if (error) return;
		// Avant de sauvegarder la carte, il faut qu'on creer l'objet carte

		const newCard: CardInput = {
			expiredAt: convertMMYYToDate(expiration.value),
			last_four: number.value,
			brand: brand.value as BrandName,
			cardColor: editMode ? card!.cardColor : generateCardColor(),
		};
		// Maintenant on va pouvoir sauvegarder les donnees
		if (this.props.editMode) {
			this.props.editCardRequest({
				...newCard,
				id: this.props.card!.cardId,
			});
		} else {
			this.props.addCardRequest(newCard);
		}

		// On ferme le modal
		Modal.instance.hide();
	};

	onDelete = () => {
		const { card } = this.props;

		this.props.deleteCard(card!.cardId);

		Modal.instance.hide();
	};

	render() {
		const { card, editMode } = this.props;

		const { number, brand, expiration } = this.state;

		return (
			<CardFormS>
				<CardFormContentS>
					<CardFormInputsS>
						<CardNumberInputS
							onChange={this.handleCardNumberChange}
							name="number"
							placeholder="Numéro de carte (4 chiffres)"
							value={number.value}
							error={number.error}
						/>
						<BrandSelector
							onChange={this.handleCardBrandChange}
							selectedBrandName={card ? card.cardBrand : null}
							error={brand.error}
						/>
						<CardExpirationInputS
							onChange={this.handleCardExpirationChange}
							name="expiration"
							placeholder="Expiration"
							value={expiration.value}
							error={expiration.error}
						/>
					</CardFormInputsS>
					{editMode ? (
						<EditButtonsContainer>
							<CustomButton
								onClick={this.onDelete}
								color={Colors.red}
							>
								Supprimer
							</CustomButton>
							<CustomButton
								onClick={this.onValidate}
								color={Colors.orange}
							>
								Modifier
							</CustomButton>
						</EditButtonsContainer>
					) : (
						<CustomButton
							onClick={this.onValidate}
							color={Colors.green}
						>
							Ajouter
						</CustomButton>
					)}
				</CardFormContentS>
			</CardFormS>
		);
	}
}

export default connect(
	null,
	{
		addCardRequest,
		editCardRequest,
		deleteCard,
	},
)(CardForm);
