import styled from "styled-components";

/* Images */
import { ReactComponent as AddLogo } from "../../../ressources/assets/add.svg";

/* Colors */
import { Colors } from "../../../ressources/styles/colors.styles";

export const CardsControllerS = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    & button {
        margin-top: 2rem;
    }
`;

export const CardsControllerContainerS = styled.div`
    height: 70%;
    width: 50%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
`;

export const EmptyWalletContainerS = styled.div`
    height: 500px;
    width: 450px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
`;

export const EmptyWalletLogoS = styled.img`
    height: 270px;
    width: 270px;
`;

export const EmptyWalletTextS = styled.span`
    font-family: "Open Sans condensed", sans-serif;
    font-size: 1.4rem;
    text-align: center;
`;

export const AddCardButtonS = styled(AddLogo)`
    height: 50px;
    width: 50px;
    fill: ${Colors.darkGreen};
    cursor: pointer;
    margin-top: 4rem;
`;
