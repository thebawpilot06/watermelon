/* Redux */
import { connect } from "react-redux";

// Selectors
import { selectAreCardsFetching } from "../../../redux/cards/cards.selectors";

/* Components */

// HOC
import WithSpinner from "../../spinner/with-spinner.component";

// Own
import CardsController from "./cards-controller.component";

/* Interfaces */
// eslint-disable-next-line no-unused-vars
import IRootState from "../../../redux/root/root.types";

interface CardsControllerContainerProps {
	isLoading: boolean;
}

const mapStateToProps = (state: IRootState): CardsControllerContainerProps => (
	{ isLoading: selectAreCardsFetching(state) }
	);

export default connect(
	mapStateToProps,
	{},
)(WithSpinner(CardsController));
