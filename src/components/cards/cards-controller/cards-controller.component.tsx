import React from "react";

/* Redux */
import { connect } from "react-redux";

/* Utils */
// Lodash
import { values, isEmpty } from "lodash";

// Selectors
import {
	selectCards,
	selectCardError,
} from "../../../redux/cards/cards.selectors";

/* Images */
import WalletPic from "../../../ressources/assets/wallet.png";

/* Components */

// Styles
import {
	CardsControllerContainerS,
	CardsControllerS,
	EmptyWalletContainerS,
	EmptyWalletLogoS,
	EmptyWalletTextS,
	AddCardButtonS,
} from "./cards-controller.styles";

// Own
import Modal from "../../generic/modal/modal.component";
import Notification, { NOTIFACTION_BG } from "../../generic/notification/notification.component";
import CardPictureAnimator from "../card-picture-animator/card-picture-animator.component";
import CardViewList from "../card-view-list/card-view-list.component";
import CardForm from "../card-form/card-form.component";

/* Interfaces */
// eslint-disable-next-line no-unused-vars
import IRootState from "../../../redux/root/root.types";
// eslint-disable-next-line no-unused-vars
import { CardData } from "../../../ressources/interfaces/generic.interfaces";
import { CardError } from "../../../redux/cards/cards.types";

interface CardsControllerProps {
	cards: {
		[id: string]: CardData;
	} | null;
	error: CardError | null;
}

interface CardsControllerState {
	selectedCard: CardData | null;
	numberOfCards: number | null;
	selectedCardIndex: number | null;
}

class CardsController extends React.Component<
	CardsControllerProps,
	CardsControllerState
> {
	constructor(props: CardsControllerProps) {
		super(props);

		this.state = {
			selectedCardIndex: null,
			numberOfCards: null,
			selectedCard: null,
		};
	}

	static getDerivedStateFromProps(
		props: CardsControllerProps,
		state: CardsControllerState,
	) {
		if (props.cards !== null && state.selectedCard === null) {
			return {
				selectedCard: values(props.cards)[0],
				numberOfCards: Object.keys(props.cards).length,
				selectedCardIndex: 0,
			};
		}

		if (
			!isEmpty(props.cards) &&
			Object.keys(props.cards!).length < state.numberOfCards!
		) {
			const newNumberOfCards = Object.keys(props.cards!).length;
			return {
				selectedCard: values(props.cards)[0],
				numberOfCards: newNumberOfCards,
				selectedCardIndex: 0,
			};
		}

		if (!isEmpty(props.cards) && Object.keys(props.cards!).length === 1) {
			return {
				selectedCard: values(props.cards)[0],
				numberOfCards: 1,
				selectedCardIndex: 0,
			};
		}

		if (
			values(props.cards)[state.selectedCardIndex as number] !==
			state.selectedCard
		) {
			return {
				selectedCard: values(props.cards)[
					state.selectedCardIndex as number
				],
			};
		}

		return null;
	}

	componentDidUpdate(prevProps: CardsControllerProps) {
		if (this.props.error && !prevProps.error) {
			if (this.props.error === CardError.CARD_ALREADY_EXISTS) {
				Notification.instance.show(
					NOTIFACTION_BG.WARNING,
					"🚫 Vous possédez déjà cette carte 🚫",
				);
			}
		}
	}

	handleCardSelected = (cardIndex: number) => {
		const { cards } = this.props;

		this.setState({
			selectedCard: values(cards)[cardIndex],
			selectedCardIndex: cardIndex,
		});
	};

	openAddCardModal = () => {
		Modal.instance.configure({
			bodyContent: <CardForm />,
			hideCloseButton: false,
		});

		Modal.instance.show();
	};

	render() {
		const { cards } = this.props;
		const { selectedCard, selectedCardIndex } = this.state;

		return (
			<CardsControllerS>
				{cards && !isEmpty(cards) ? (
					<CardsControllerContainerS>
						<CardPictureAnimator newCard={selectedCard!} />
						<CardViewList
							onSelect={this.handleCardSelected}
							cards={cards!}
							cardSelectedIndex={selectedCardIndex!}
						/>
					</CardsControllerContainerS>
				) : (
					<EmptyWalletContainerS>
						<EmptyWalletLogoS src={WalletPic} />
						<EmptyWalletTextS>
							Il semblerait que vous n'ayez toujours pas ajouté de
							cartes à votre compte Watermelon.
							<br />
							<br />
							Pour rajouter des cartes à votre compte, il vous
							suffit simplement de cliquer sur le bouton ci
							dessous
							<br />
						</EmptyWalletTextS>
					</EmptyWalletContainerS>
				)}
				<AddCardButtonS onClick={this.openAddCardModal} />
			</CardsControllerS>
		);
	}
}

const mapStateToProps = (state: IRootState): CardsControllerProps => ({
	cards: selectCards(state),
	error: selectCardError(state),
});

export default connect(
	mapStateToProps,
	{},
)(CardsController);
