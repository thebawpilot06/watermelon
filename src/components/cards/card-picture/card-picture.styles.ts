import styled, { css } from "styled-components";

const containerCss = css`
    text-transform: uppercase;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: flex-start;
`;

export const CardPictureS = styled.div<{cardColor: string}>`
    position: relative;
    width: 400px;
    height: 240px;
    border-radius: 30px;
    background-color: ${props => props.cardColor};
    font-family: "Ubuntu", sans-serif;
    color: white;
    
    & svg {
        position: absolute;
        height: 50px;
        width: 50px;
        bottom: 20px;
        right: 30px;
    }
`;

export const CardNumberS = styled.div`
    position: absolute;
    left: 40px;
    top: 50%;
    transform: translateY(-50%);
    font-size: 1.5rem;

    & span {
        margin-right: 1rem;
    }
`;

export const CardContainersS = styled.div`
    position: absolute;
    bottom: 20px;
    left: 40px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 200px;
`;

export const CardExpirationContainerS = styled.div`
    ${containerCss}

    & span {
        opacity: 0.6
    }
`;
