import React from "react";

/* Components */

// Logo
import { ReactComponent as MasterCard } from "../../../ressources/assets/master-card.svg";
import { ReactComponent as Visa } from "../../../ressources/assets/visa.svg";
import { ReactComponent as UnionPay } from "../../../ressources/assets/union-pay.svg";
import { ReactComponent as AmericanExpress } from "../../../ressources/assets/american-express.svg";
import { ReactComponent as JCB } from "../../../ressources/assets/jcb.svg";

// Styles
import {
	CardContainersS,
	CardPictureS,
	CardNumberS,
	CardExpirationContainerS,
} from "./card-picture.styles";

/* Interfaces */
import { BrandName } from "../../../database/Cards.database";

interface CardPictureProps {
	cardNumber: string;
	cardExpiration: string;
	cardColor: string;
	cardBrand: BrandName;
}

const CardPicture: React.FunctionComponent<CardPictureProps> = ({
	cardNumber,
	cardExpiration,
	cardColor,
	cardBrand,
}) => {
	const formatCardNumber = (number: string) => {
		const numbersArray = [];

		numbersArray.push(
			<span key="number-part-1">****</span>,
		);
		numbersArray.push(
			<span key="number-part-2">****</span>,
		);
		numbersArray.push(
			<span key="number-part-3">****</span>,
		);
		numbersArray.push(
			<span key="number-part-4">{number.slice(0, 4)}</span>,
		);

		return numbersArray;
	};

	const getBrandLogo = (brand: BrandName) => {
		switch (brand) {
			case BrandName.MasterCard:
				return <MasterCard />;
			case BrandName.Visa:
				return <Visa />;
			case BrandName.UnionPay:
				return <UnionPay />;
			case BrandName.AmericanExpress:
				return <AmericanExpress />;
			case BrandName.JCB:
				return <JCB />;
			default:
				return null;
		}
	};

	return (
		<CardPictureS cardColor={cardColor}>
			<CardNumberS>{formatCardNumber(cardNumber)}</CardNumberS>
			<CardContainersS>
				<CardExpirationContainerS>
					<span>VALIDE THU</span>
					{cardExpiration}
				</CardExpirationContainerS>
			</CardContainersS>
			{getBrandLogo(cardBrand)}
		</CardPictureS>
	);
};

export default CardPicture;
