import React, { Fragment } from "react";

/* Components */

// Styles
import { CardViewListS, SeparationLineS } from "./card-view-list.styles";

// Own
import Modal from "../../generic/modal/modal.component";
import CardForm from "../card-form/card-form.component";
import CardView from "../card-view/card-view.component";

/* Interfaces */
import { CardData } from "../../../ressources/interfaces/generic.interfaces";

interface CardViewListActionProps {
	onSelect: (idx: number) => void;
}

interface CardViewListProps {
	cards: {
		[id: string]: CardData;
	};
	cardSelectedIndex: number;
}

class CardViewList extends React.Component<
	CardViewListActionProps & CardViewListProps
> {
	openEditModal = (card: CardData) => {
		Modal.instance.configure({
			bodyContent: <CardForm card={card} editMode />,
			hideCloseButton: false,
		});

		Modal.instance.show();
	};

	render() {
		const { cards, cardSelectedIndex } = this.props;

		return (
			<CardViewListS>
				{Object.values(cards).map((card, idx) => {
					return (
						// eslint-disable-next-line react/no-array-index-key
						<Fragment key={idx}>
							<CardView
								key={card.cardNumber}
								cardNumber={card.cardNumber}
								cardColor={card.cardColor}
								cardExpiration={card.cardExpiration}
								onClick={() => this.props.onSelect(idx)}
								onEditClicked={() => this.openEditModal(card)}
								selected={idx === cardSelectedIndex}
								editMode
								/* eslint-disable-next-line react/jsx-props-no-spreading */
								{...card}
							/>
							<SeparationLineS
								/* eslint-disable-next-line react/no-array-index-key */
								key={`separation-${idx}`}
								className="separation-line"
							/>
						</Fragment>
					);
				})}
			</CardViewListS>
		);
	}
}

export default CardViewList;
