import styled from "styled-components";

/* Colors */
import { Colors } from "../../../ressources/styles/colors.styles";

export const CardViewListS = styled.div`
    height: 280px;
    width: 100%;
    margin-top: 5rem;
    padding-bottom: 50px;
    overflow: scroll;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;


    & .separation-line:last-child {
        display: none;
    }
`;

export const SeparationLineS = styled.div`
    height: 2px;
    width: 60%;
    margin: 2rem 0;
    background-color: ${Colors.lightGrey};
`;
