import React from "react";

/* ClassNames */
import classNames from "classnames";

/* Components */

// Logo
import { ReactComponent as EditLogo } from "../../../ressources/assets/edit.svg";

// Styles
import {
	CardViewS,
	CardViewContentS,
	BankCardColorS,
	BankCardNumberS,
	EditContainerS,
} from "./card-view.styles";

/* Interfaces */
interface CardViewProps {
	onClick: () => void;
	onEditClicked?: () => void;
	selected: boolean;
	borderWithErrorColor?: boolean;
	cardColor: string;
	cardNumber: string;
	cardExpiration: string;
	editMode?: true;
}

const CardView: React.FunctionComponent<CardViewProps> = (
	{ onClick,
		onEditClicked,
		selected,
		borderWithErrorColor,
		cardColor,
		cardNumber,
		cardExpiration,
		editMode,
	},
) => {
	const formatCardNumber = (number: string) => {
		const cardNumberF = [];

		cardNumberF.push(<span key="dot-1">····</span>);
		cardNumberF.push(<span key="dot-2">····</span>);
		cardNumberF.push(<span key="dot-3">····</span>);
		cardNumberF.push(
			<span key="number-part-4">{number}</span>,
		);

		return cardNumberF;
	};

	const cardViewClassNames = classNames("card", { selected });

	return (
		<CardViewS
			className={cardViewClassNames}
			onClick={onClick}
			borderWithErrorColor={borderWithErrorColor}
		>
			<CardViewContentS>
				<BankCardColorS cardColor={cardColor} />
				<BankCardNumberS>
					{formatCardNumber(`${cardNumber}`)}
				</BankCardNumberS>
				{cardExpiration}
			</CardViewContentS>
			{editMode ? (
				<EditContainerS className="edit" onClick={onEditClicked}>
					<EditLogo />
				</EditContainerS>
			) : null}
		</CardViewS>
	);
};

export default CardView;
