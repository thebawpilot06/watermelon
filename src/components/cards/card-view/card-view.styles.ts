import styled from "styled-components";

/* Colors */
import { Colors } from "../../../ressources/styles/colors.styles";

const getBorderColor = (borderWithErrorColor?: boolean) => {
    if (borderWithErrorColor) return Colors.lightRed;

    return Colors.lightBlue;
};

export const CardViewS = styled.div<{borderWithErrorColor?: boolean}>`
    position: relative;
    margin-left: 5px;
    min-height: 70px;
    width: 450px;
    border-radius: 10px;
    border-style: solid;
    border-color: transparent;
    background-color: white;
    cursor: pointer;
    font-family: "Ubuntu", sans-serif;
    font-size: 1.2rem;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    -webkit-box-shadow: 0px 6px 4px 0px rgba(184,184,184,1);
    -moz-box-shadow: 0px 6px 4px 0px rgba(184,184,184,1);
    box-shadow: 0px 6px 4px 0px rgba(184,184,184,1);
     -webkit-transition: border-width 0.8s, border-color 0.6s ease-in-out, display 6s ease-in; /* Safari prior 6.1 */
    transition: border-width 0.8s, border-color 0.6s ease-in-out, display 6s ease-in;

    &.selected {
        border-width: 3px;
        border-color: ${props => getBorderColor(props.borderWithErrorColor)};

        .edit {
            display: flex;
            justify-content: center;
            align-items: center;
        }
    }
`;

export const CardViewContentS = styled.div`
    width: 80%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

export const BankCardColorS = styled.div<{cardColor: string}>`
    height: 30px;
    width: 30px;
    border-radius: 30px;
    background-color: ${props => props.cardColor};
`;

export const BankCardNumberS = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;

    & span {
        margin-right: 1rem;
        font-size: 1.9rem;
    }

    & span:last-child{
        margin-right: 0;
        font-size: 1.2rem;
    }

`;

export const EditContainerS = styled.div`
    position: absolute;
    right: 0;
    transform: translateX(50%);
    height: 45px;
    width:  45px;
    border-radius: 40px;
    background-color: ${Colors.lightBlue};
    cursor: pointer;
    display: none;

    & svg {
        height: 25px;
        width: 25px;
        fill: white;
    }

`;
