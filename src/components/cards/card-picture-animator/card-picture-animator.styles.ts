import styled from "styled-components";

export const CardPictureAnimatorS = styled.div`
    height: 240px;
    width: 400px;
    position:relative;
`;

export const CardPictureContainerS = styled.div`
    position: absolute;

    &.twirl-enter {
        transform: rotateX(90deg);
    }

    &.twirl-enter.twirl-enter-active {
        transform: rotateX(0deg);
        transition: all 200ms 200ms ease-out;
    }

    &.twirl-leave {
        transform: rotateX(0deg);
    }

    &.twirl-leave-active {
        transform: rotateX(90deg);
        transition: all 200ms ease-in;
    }
`;
