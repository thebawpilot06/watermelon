import React from "react";

/* Css Transition */
import ReactCssTransitionGroup from "react-addons-css-transition-group";

/* Components */

// Styles
import {
    CardPictureAnimatorS,
    CardPictureContainerS,
} from "./card-picture-animator.styles";


// Own
import CardPicture from "../card-picture/card-picture.component";

/* Interfaces */
import { BrandName } from "../../../database/Cards.database";

interface Card {
    cardNumber: string;
    cardExpiration: string;
    cardColor: string;
    cardBrand: BrandName;
}

interface CardPictureAnimatorProps {
    newCard: Card;
}

const CardPictureAnimator: React.FunctionComponent<CardPictureAnimatorProps> = ({ newCard }) => {
    const getNewCard = () => [
            <CardPictureContainerS key={`${newCard.cardNumber}-${newCard.cardExpiration}-${newCard.cardBrand}`}>
                <CardPicture
                    cardNumber={newCard.cardNumber}
                    cardExpiration={newCard.cardExpiration}
                    cardColor={newCard.cardColor}
                    cardBrand={newCard.cardBrand}
                />
            </CardPictureContainerS>,
        ];
    return (
        <CardPictureAnimatorS>
            <ReactCssTransitionGroup
                transitionName="twirl"
                transitionEnter
                transitionLeave
                transitionEnterTimeout={400}
                transitionLeaveTimeout={200}
            >
                {getNewCard()}
            </ReactCssTransitionGroup>
        </CardPictureAnimatorS>
    );
};

export default CardPictureAnimator;
