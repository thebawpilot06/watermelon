import styled from "styled-components";

/* Style */

// Box
import boxCss from "../../../ressources/styles/box.styles";

export const SoldS = styled.div`
    height: 100%;
    width: 100%;
    border : thick double #4287f5;
    border-radius: ${boxCss.borderRadius};
    text-align : center;
    font-family: 'Ubuntu', sans-serif;
    font-size: 1.4rem;
`;

export const AmountS = styled.p`
    font-family: 'Ubuntu', sans-serif;
    font-size: 1.8rem;
`;
