/* Redux */
import { connect } from "react-redux";

// Selectors
import { selectWalletFetching } from "../../../redux/wallet/wallet.selectors";

/* Components */

// HOC
import WithSpinner from "../../spinner/with-spinner.component";

// Own
import Sold from "./sold.component";

/* Interfaces */
// eslint-disable-next-line no-unused-vars
import IRootState from "../../../redux/root/root.types";

interface SoldContainerProps {
    isLoading: boolean;
}

const mapStateToProps = (state: IRootState): SoldContainerProps => (
  { isLoading: selectWalletFetching(state) }
  );

export default connect(mapStateToProps, { })(WithSpinner(Sold));
