import React from "react";

/* Redux */
import { connect } from "react-redux";

// Selectors
import { selectWalletAmount } from "../../../redux/wallet/wallet.selectors";

/* Components */
// styles
import {
  SoldS,
  AmountS,
} from "./sold.styles";

/* Interfaces */
// eslint-disable-next-line no-unused-vars
import IRootState from "../../../redux/root/root.types";

interface SoldProps {
    amount: number;
}

class Sold extends React.Component<SoldProps> {
  // eslint-disable-next-line no-useless-constructor
  constructor(props: SoldProps) {
    super(props);
  }

  render() {
    return (
      <SoldS>
        <p> Votre solde : </p>
        <AmountS>
          { this.props.amount }
          €
        </AmountS>
      </SoldS>
    );
  }
}

const mapStateToProps = (state: IRootState): SoldProps => ({ amount: selectWalletAmount(state) });

export default connect(mapStateToProps, {})(Sold);
