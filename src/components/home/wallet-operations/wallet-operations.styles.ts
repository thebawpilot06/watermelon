import styled from "styled-components";

export const ActionsList = styled.div`
    width: 800px;
    position :relative;
    padding-bottom : 2px;
    padding-top : 2px;
    margin-left: 5px;
    margin-top : 5px;
    margin-bottom : 15px;
    text-align : center;
    font-family: 'Ubuntu', sans-serif;
    font-size: 1.4rem;
    
    & .boutonTopUp {
    font:bold 25px "Roboto Slab";
    background: #207561;
    color:#fff;
    width:400px;
    height : 50px;
    border-radius:12px 0 12px 0;
    cursor: pointer;
    }
    
    & .boutonWithdraw {
    font:bold 25px "Roboto Slab";
    background:#589167;
    color:#fff;
    width:400px;
    height : 50px;
    border-radius:12px 0 12px 0;
    cursor: pointer;
    }
    
    & .boutonTransfert {
    font:bold 25px "Roboto Slab";
    background:#dc5353;
    color:#fff;
    width:400px;
    height : 55px;
    border-radius:12px 0 12px 0;
    cursor: pointer;
    }
    
    & .textbutton {
    text-align : center;
    }
`;

export const IconTopUpS = styled.span`
    position: relative;
    left:-58px;
    top:5px
    
    & .topup-logo {
            height: 28px;
            width: 28px;
        }
`;

export const IconWithdrawS = styled.span`
    position: relative;
    left:-60px;
    top:5px
    
    & .withdraw-logo {
            height: 28px;
            width: 28px;
        }
`;

export const IconTransfertS = styled.span`
    position: relative;
    left:-60px;
    top:5px;    
    
    & .transfert-logo {
            height: 30px;
            width: 30px;  
        }
`;
