import React from "react";

/* Redux */
import { connect } from "react-redux";

// Selectors
import { selectWalletAmount } from "../../../redux/wallet/wallet.selectors";

/* Components */

// Pictures
import { ReactComponent as IconTransfert } from "../../../ressources/assets/transfer.svg";
import { ReactComponent as IconWithdraw } from "../../../ressources/assets/withdraw.svg";
import { ReactComponent as IconTopUp } from "../../../ressources/assets/topup.svg";

// Styles
import {
  ActionsList,
  IconTopUpS,
  IconTransfertS,
  IconWithdrawS,
} from "./wallet-operations.styles";

// Own
import Modal from "../../generic/modal/modal.component";
import CardsAndAmountSelection from "../../wallet-operations/cards-and-amount-selection/cards-and-amount-selection.component";
import TransfertBody from "../../wallet-operations/transfert-money/transfert-money.component";

/* Interfaces */
// eslint-disable-next-line no-unused-vars
import IRootState from "../../../redux/root/root.types";

interface AmountWalletProps {
  amount: number;
}

class WalletOperations extends React.Component<AmountWalletProps> {

  onClickWithdraw = () => {
    Modal.instance.configure({
      headerContent: undefined,
      headerContentPos: undefined,
      headerTextColor: "blue",
      bodyContent: <CardsAndAmountSelection headerText="Retirer de l'argent" actionToDo="withdraw" />,
      bodyTextColor: "black",
      footerContent: undefined,
      footerContentPos: undefined,
      footerTextColor: "red",
      hideCloseButton: false,
    });
    Modal.instance.show();
  };

  onClickTopUp = () => {
    Modal.instance.configure({
      headerContent: undefined,
      headerContentPos: undefined,
      headerTextColor: "blue",
      bodyContent: <CardsAndAmountSelection actionToDo="topup" headerText="Ajouter de l'argent" />,
      bodyTextColor: "black",
      footerContent: undefined,
      footerContentPos: undefined,
      footerTextColor: "red",
      hideCloseButton: false,
    });
    Modal.instance.show();
  };

  onClickTransfert = () => {
    Modal.instance.configure({
      headerContent: undefined,
      headerContentPos: undefined,
      headerTextColor: "blue",
      bodyContent: <TransfertBody walletAmount={this.props.amount} />,
      bodyTextColor: "black",
      footerContent: undefined,
      footerContentPos: undefined,
      footerTextColor: "red",
      hideCloseButton: false,
    });
    Modal.instance.show();
  };

  render() {
    return (
      <ActionsList>
        <p>
          <button className="boutonTopUp" onClick={this.onClickTopUp} type="button">
            <IconTopUpS>
              <IconTopUp className="topup-logo" />
            </IconTopUpS>
            <span className="textbutton">Ajouter de l'argent</span>
          </button>
        </p>
        <p>
          <button className="boutonWithdraw" onClick={this.onClickWithdraw} type="button">
            <IconWithdrawS>
              <IconWithdraw className="withdraw-logo" />
            </IconWithdrawS>
            <span className="textbutton">Retirer de l'argent</span>
          </button>
        </p>
        <p>
          <button className="boutonTransfert" onClick={this.onClickTransfert} type="button">
            <IconTransfertS>
              <IconTransfert className="transfert-logo" />
            </IconTransfertS>
            <span className="textbutton">Faire un virement</span>
          </button>
        </p>
      </ActionsList>
    );
  }
}

const mapStateToProps =
  (state: IRootState): AmountWalletProps => ({ amount: selectWalletAmount(state) });

export default connect(mapStateToProps, {})(WalletOperations);
