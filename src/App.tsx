/* eslint-disable no-unused-vars */
/* React */
import React, { Fragment } from "react";

/* React Router */
import {
	Route,
	Redirect,
	Switch,
} from "react-router-dom";

/* Redux */
import { connect } from "react-redux";

// Actions
import {
	updateUserAvatar,
	updateUserAvatarType,
} from "./redux/user/user.actions";

// Selectors
import { selectCurrentUser } from "./redux/user/user.selectors";

/* Utils */
import { reloadDbDocuments } from "./utils/database/database.utils";

/* Components */

// Logo
import { ReactComponent as CameraLogo } from "./ressources/assets/camera.svg";

// Styles
import { GlobalStyle } from "./global.styles";

// Own
import Modal from "./components/generic/modal/modal.component";
import DropZone from "./components/generic/drop-zone/drop-zone.component";
import Header from "./components/header/header.component";
import Notification, { NOTIFACTION_BG } from "./components/generic/notification/notification.component";

/* Routes */
import AuthenticationPage from "./pages/authentication/authentication-page.component";
import HomePage from "./pages/home/home-page.component";
import CardsPage from "./pages/cards/cards-page.component";
import AccountPage from "./pages/account/account-page.component";

/* Interfaces */
import IRootState from "./redux/root/root.types";
import { User } from "./redux/user/user.types";

interface AppActionProps {
	updateUserAvatar: updateUserAvatarType;
}

interface AppProps {
	currentUser: User | null;
}

interface AppState {
	// eslint-disable-next-line no-tabs
	currentUser: User | null;
}

class App extends React.Component<AppProps & AppActionProps, AppState> {
	componentDidMount() {
		this.checkUserAvatar();

		reloadDbDocuments();
	}

	componentDidUpdate() {
		this.checkUserAvatar();
	}

	checkUserAvatar = () => {
		const { currentUser } = this.props;

		if (currentUser && !currentUser.avatar) {
			Modal.instance.configure({
				bodyContent: (
					<DropZone
						onFileUploaded={(file) => {
							this.props.updateUserAvatar(file);
							Modal.instance.hide();
						}}
						formatsAccepted={[
							"image/png",
							"image/jpg",
							"image/jpeg",
						]}
					>
						<p>
							En raison de la norme européenne UE2932314XAJ, nous
							sommes obligés de vérifier votre identité.
							<br />
							<br />
							Ainsi pour pouvoir poursuivre votre inscription il
							vous faudra uploader une image de vous.
							<br />
							<br />
							Les formats suivant sont acceptés: png, jpeg, jpg
						</p>
						<CameraLogo />
					</DropZone>
				),
				hideCloseButton: true,
			});

			Modal.instance.show();
		}
	};

	render() {
		return (
			<div className="App">
				<GlobalStyle />
				{!this.props.currentUser ? (
					<Fragment>
						<Route
							exact
							path="/authentication"
							component={AuthenticationPage}
						/>
						<Redirect from="/" to="/authentication" />
					</Fragment>
				) : (
					<Fragment>
						<Header user={this.props.currentUser} />
						<Switch>
							<Route exact path="/" component={HomePage} />
							<Route exact path="/cards" component={CardsPage} />
							<Route exact path="/account" component={AccountPage} />
							<Redirect to="/" />
						</Switch>
					</Fragment>
				)}
				<Modal />
				<Notification />
			</div>
		);
	}
}

const mapStateToProps = (state: IRootState): AppProps => (
	{ currentUser: selectCurrentUser(state) }
	);

export default connect(
	mapStateToProps,
	{ updateUserAvatar },
)(App);
