/* eslint-disable no-unused-vars */
// Types
import {
  IWalletState,
  FETCH_WALLET_DATA_START,
  FETCH_WALLET_DATA_SUCCESS,
  WITHDRAW_SUCCESS,
  TOP_UP_SUCCESS,
  TRANSFERT_SUCCESS,
  RESET_WALLET_REDUCER,
  WalletActionTypes,
} from "./wallet.types";

// Initial State
const INITIAL_STATE: IWalletState = {
    id: null,
    amount: 0,
    isWalletDataFetching: false,
    expiration: null,
};

// eslint-disable-next-line max-len
const walletReducer = (state: IWalletState = INITIAL_STATE, action: WalletActionTypes): IWalletState => {
    switch (action.type) {
        case FETCH_WALLET_DATA_START:
            return {
                ...state,
                isWalletDataFetching: true,
            };
        case FETCH_WALLET_DATA_SUCCESS:
            return {
                id: action.payload.id,
                amount: action.payload.amount,
                isWalletDataFetching: false,
                expiration: action.payload.expiration,
            };
        case WITHDRAW_SUCCESS:
            return {
                ...state,
                amount: state.amount - action.payload,
            };
        case TOP_UP_SUCCESS:
            return {
                ...state,
                amount: state.amount + action.payload,
            };
        case TRANSFERT_SUCCESS:
            return {
                ...state,
                amount: state.amount - action.payload,
            };
        case RESET_WALLET_REDUCER:
            return { ...INITIAL_STATE };
        default:
            return state;
    }
};

export default walletReducer;
