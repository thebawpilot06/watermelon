// Types
import {
    FETCH_WALLET_DATA_REQUEST,
    FETCH_WALLET_DATA_START,
    FETCH_WALLET_DATA_SUCCESS,
    WITHDRAW_REQUEST,
    WITHDRAW_SUCCESS,
    TOP_UP_REQUEST,
    TOP_UP_SUCCESS,
    TRANSFERT_REQUEST,
    TRANSFERT_SUCCESS,
    RESET_WALLET_REDUCER,
    // eslint-disable-next-line no-unused-vars
    WalletActionTypes,
} from "./wallet.types";

// Action Types
export type withdrawRequestType = {
    (amount: number): WalletActionTypes;
}

export type topUpRequestType = {
    (amount: number): WalletActionTypes;
}

export type transfertRequestType = {
    (creditedAccount: number, amount: number): WalletActionTypes;
}

// Actions
// eslint-disable-next-line max-len
export const fetchWalletDataRequest = (): WalletActionTypes => ({ type: FETCH_WALLET_DATA_REQUEST });

export const fetchWalletDataStart = (): WalletActionTypes => ({ type: FETCH_WALLET_DATA_START });

// eslint-disable-next-line max-len
export const fetchWalletDataSuccess = (id: number, amount: number, expiration: Date): WalletActionTypes => ({
    type: FETCH_WALLET_DATA_SUCCESS,
    payload: {
        id,
        amount,
        expiration,
    },
});

export const withdrawRequest = (amount: number): WalletActionTypes => ({
    type: WITHDRAW_REQUEST,
    payload: amount,
});

export const withdrawSuccess = (amount: number): WalletActionTypes => ({
    type: WITHDRAW_SUCCESS,
    payload: amount,
});

export const topUpRequest = (amount: number): WalletActionTypes => ({
    type: TOP_UP_REQUEST,
    payload: amount,
});

export const topUpSuccess = (amount: number): WalletActionTypes => ({
    type: TOP_UP_SUCCESS,
    payload: amount,
});

export const transfertRequest = (creditedUserId: number, amount: number): WalletActionTypes => ({
    type: TRANSFERT_REQUEST,
    payload: {
     creditedUserId,
     amount,
    },
});

export const transfertSuccess = (amount: number): WalletActionTypes => ({
    type: TRANSFERT_SUCCESS,
    payload: amount,
});

export const resetWalletReducer = (): WalletActionTypes => ({ type: RESET_WALLET_REDUCER });
