/* Interfaces */
export interface IWalletState {
    id: number | null;
	amount: number;
    isWalletDataFetching: boolean;
    expiration: Date | null;
}

/* Action Types */
export const FETCH_WALLET_DATA_REQUEST = "FETCH_WALLET_DATA_REQUEST";
export const FETCH_WALLET_DATA_START = "FETCH_WALLET_DATA_START";
export const FETCH_WALLET_DATA_SUCCESS = "FETCH_WALLET_DATA_SUCCESS";

export const WITHDRAW_REQUEST = "WITHDRAW_REQUEST";
export const WITHDRAW_SUCCESS = "WITHDRAW_SUCCESS";

export const TOP_UP_REQUEST = "TOP_UP_REQUEST";
export const TOP_UP_SUCCESS = "TOP_UP_SUCCESS";

export const TRANSFERT_REQUEST = "TRANSFERT_REQUEST";
export const TRANSFERT_SUCCESS = "TRANSFERT_SUCCESS";

export const RESET_WALLET_REDUCER = "RESET_WALLET_REDUCER";

export type FetchWalletDataRequest = {
	type: typeof FETCH_WALLET_DATA_REQUEST;
};

export type FetchWalletDataStart = {
	type: typeof FETCH_WALLET_DATA_START;
};

export type FetchWalletDataSuccess = {
	type: typeof FETCH_WALLET_DATA_SUCCESS;
	payload: {
        id: number;
        amount: number;
        expiration: Date;
    };
};

export type WithdrawRequest = {
    type: typeof WITHDRAW_REQUEST;
    payload: number,
}

export type WithdrawSuccess = {
    type: typeof WITHDRAW_SUCCESS;
    payload: number;
}

export type TopUpRequest = {
    type: typeof TOP_UP_REQUEST;
    payload: number;
}

export type TopUpSuccess = {
    type: typeof TOP_UP_SUCCESS;
    payload: number;
}

export type TransfertRequest = {
    type: typeof TRANSFERT_REQUEST;
    payload: {
        creditedUserId: number;
        amount: number;
    }
}

export type TransfertSuccess = {
    type: typeof TRANSFERT_SUCCESS;
    payload: number;
}

export type ResetWalletReducer = {
    type: typeof RESET_WALLET_REDUCER;
}

export type WalletActionTypes =
	| FetchWalletDataRequest
	| FetchWalletDataStart
    | FetchWalletDataSuccess
    | WithdrawRequest
    | WithdrawSuccess
    | TopUpRequest
    | TopUpSuccess
    | TransfertRequest
    | TransfertSuccess
    | ResetWalletReducer
