/* eslint-disable no-unused-vars,arrow-parens,no-shadow */
import { createSelector } from "reselect";

// Root interface
import IRootState from "../root/root.types";

const selectWalletReducer = (state: IRootState) => state.walletReducer;

export const selectWalletAmount = createSelector(
  [selectWalletReducer],
  walletReducer => walletReducer.amount,
);

export const selectWalletFetching = createSelector(
  [selectWalletReducer],
  walletReducer => walletReducer.isWalletDataFetching,
);
