/* eslint-disable no-unused-vars,max-len */
// Redux-Saga
import {
  take,
  takeLatest,
  put,
  all,
  call,
  select,
  delay,
} from "redux-saga/effects";

// Types
import {
  FETCH_WALLET_DATA_REQUEST,
  WITHDRAW_REQUEST,
  TOP_UP_REQUEST,
  TRANSFERT_REQUEST,
  WithdrawRequest,
  TopUpRequest,
  TransfertRequest,
  WalletActionTypes,
// eslint-disable-next-line import/no-duplicates
} from "./wallet.types";

// Actions
import {
  fetchWalletDataStart,
  fetchWalletDataSuccess,
  withdrawSuccess,
  topUpSuccess,
  transfertSuccess,
} from "./wallet.actions";

/* Utils */

// Wallet
import { getWalletFor } from "../../utils/database/database-wallet.utils";

// Date
import {
  checkExpirationPassed,
  setExpiration,
} from "../../utils/date/date.utils";

// PayIn
import {
  addPayIn,
  getPayInsFor,
} from "../../utils/database/database-pay-in.utils";

// PayOut
import {
  addPayOutFor,
  getPayOutsFor,
} from "../../utils/database/database-pay-out.utils";

// Transfer
import {
  addTransfert,
  getCreditedTransfersFor,
  getDebitedTransfersFor,
} from "../../utils/database/database-transfer.utils";

/* Interfaces */
import IRootState from "../root/root.types";
import { User } from "../user/user.types";
// eslint-disable-next-line import/no-duplicates
import { IWalletState } from "./wallet.types";
import { WalletDB } from "../../database/Wallets.databse";
import { PayInDB } from "../../database/PayIns.database";
import { PayOutDB } from "../../database/PayOuts.database";
import { TransferDB } from "../../database/Transfers.database";

/* Saga Workers */
function* fetchWalletData() {
  // On verifie tout d'abord si l'expiration au niveau du reducer est passe
  const walletReducer: IWalletState = yield select((state: IRootState) => state.walletReducer);

  if (!checkExpirationPassed(walletReducer.expiration)) return;

  // On indique que l'on est en train de fetcher les informations concernant notre compte
  yield put(fetchWalletDataStart());

  // On va simuler un petit temps d'attende comme dans les API classique
  yield delay(1600);

  // On va tout d'abord recuperer l'id de l'utilisateur
  const user: User = yield select((state: IRootState) => state.userReducer.currentUser);

  // Maintenant on recuperer le wallet de l'utilisateur
  const userWallet: WalletDB = yield call(getWalletFor, user.id);

  // Maintenant il nous reste plus qu'a recuperer les payins, payouts
  // et les transferts de l'utilisateur
  const payIns: PayInDB[] = yield call(getPayInsFor, userWallet.id);
  const creditedTransfers: TransferDB[] = yield call(getCreditedTransfersFor, userWallet.id);
  const payOuts: PayOutDB[] = yield call(getPayOutsFor, userWallet.id);
  const debitedTransfers: TransferDB[] = yield call(getDebitedTransfersFor, userWallet.id);

  // Ci-dessus nous avons que des listes d'objets, il nous faut une
  // valeur numerique pour faire nos calculs
  const payInsAmount =
    Object.values(payIns).reduce((total: number, payin: PayInDB) => total + payin.amount, 0);

  const creditedTransfersAmount =
    Object.values(creditedTransfers).reduce((total: number, transfer: TransferDB) => total + transfer.amount, 0);

  const payOutsAmount =
    Object.values(payOuts).reduce((total: number, payout: PayOutDB) => total + payout.amount, 0);

  const debitedTransfersAmount =
    Object.values(debitedTransfers).reduce((total: number, transfer: TransferDB) => total + transfer.amount, 0);

  // On calcule maintenant la valeur de notre wallet
  const walletAmount = payInsAmount + creditedTransfersAmount - payOutsAmount - debitedTransfersAmount;

  // On passe cette information a notre reducer
  yield put(fetchWalletDataSuccess(userWallet.id, walletAmount / 100, setExpiration(30)));
}

function* withdraw({ payload: amount }: WithdrawRequest) {
  // On a besoin d'avoir l'id du porte monnaie de notre utilisateur
  const walletId = yield select((state: IRootState) => state.walletReducer.id);

  // On va mtn sauvegarder l'operation dans la base de donnees
  yield call(addPayOutFor, walletId as number, amount * 100);

  // On met a jour le reducer
  yield put(withdrawSuccess(amount));
}

function* topUp({ payload: amount }: TopUpRequest) {
  // On a besoin de l'id du wallet de l'utilisateur
  const walletId = yield select((state: IRootState) => state.walletReducer.id);

  // Maintenant on va sauvegarder l'argent
  yield call(addPayIn, walletId, amount * 100);

  // On sauvegarde l'argent dans notre reducer
  yield put(topUpSuccess(amount));
}

function* transfert({ payload: { creditedUserId, amount } }: TransfertRequest) {
  // On a besoin de l'id du porte-monnaie de l'utilisateur
  const walletId = yield select((state: IRootState) => state.walletReducer.id);

  // On a maintenant besoin de savoir l'id du porte monnaie de l'utilisateur à qui on envoie l'argentt
  const creditedWallet: WalletDB = yield call(getWalletFor, creditedUserId);

  // On sauvegarde maintenant l'operation dans la base de donnees
  yield call(addTransfert, walletId, creditedWallet.id, amount * 100);

  // On met a jour notre reducer
  yield put(transfertSuccess(amount));
}

/* Saga Watchers */
function* onFetchWalletDataRequest() {
  yield takeLatest(FETCH_WALLET_DATA_REQUEST, fetchWalletData);
}

function* onWithdrawRequest() {
  yield takeLatest(WITHDRAW_REQUEST, withdraw);
}

function* onTopUpRequest() {
  yield takeLatest(TOP_UP_REQUEST, topUp);
}

function* onTransfertRequest() {
  yield takeLatest(TRANSFERT_REQUEST, transfert);
}

export default function* walletSagas() {
  yield all([
    call(onFetchWalletDataRequest),
    call(onWithdrawRequest),
    call(onTopUpRequest),
    call(onTransfertRequest),
  ]);
}
