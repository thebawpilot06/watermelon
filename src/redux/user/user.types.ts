/* eslint-disable no-unused-vars */
// Interfaces
import { UserDB } from "../../database/Users.database";

export type User = UserDB & {
    avatar?: string;
}

export interface IUserState {
    currentUser: User | null;
    signInError: string | null;
    signUpError: string | null;
    signOutError: string | null;
}

/* Action Types */
export const SET_CURRENT_USER = "SET_CURRENT_USER";

export const EMAIL_SIGN_IN_START = "EMAIL_SIGN_IN_START";
export const SIGN_IN_SUCCESS = "SIGN_IN_SUCCESS";
export const SIGN_IN_FAILURE = "SIGN_IN_FAILURE";
export const SIGN_OUT_START = "SIGN_OUT_START";
export const SIGN_OUT_SUCCESS = "SIGN_OUT_SUCCESS";
export const SIGN_OUT_FAILURE = "SIGN_OUT_FAILURE";
export const SIGN_UP_START = "SIGN_UP_START";
export const SIGN_UP_SUCCESS = "SIGN_UP_SUCCESS";
export const SIGN_UP_FAILURE = "SIGN_UP_FAILURE";

export const UPDATE_USER_AVATAR = "UPDATE_USER_AVATAR";

export const UPDATE_USER_DATA_REQUEST = "UPDATE_USER_DATA_START";
export const UPDATE_USER_DATA_SUCCESS = "UPDATE_USER_DATA_SUCCESS";


type SetCurrentUser = {
    type: typeof SET_CURRENT_USER;
    payload: User;
}

export type EmailSignInStart = {
    type: typeof EMAIL_SIGN_IN_START;
    payload: {
        email: string;
        password: string;
    }
}

type SignInSuccess = {
    type: typeof SIGN_IN_SUCCESS;
    payload: User;
}

type SignInFailure = {
    type: typeof SIGN_IN_FAILURE;
    payload: string;
}

type SignOutStart = {
    type: typeof SIGN_OUT_START;
}

type SignOutSuccess = {
    type: typeof SIGN_OUT_SUCCESS;
}

type SignOutFailure = {
    type: typeof SIGN_OUT_FAILURE;
    payload: string;
}

export type SignUpStart = {
    type: typeof SIGN_UP_START;
    payload: {
        displayName: string;
        email: string;
        password: string;
    }
}

type SignUpSuccess = {
    type: typeof SIGN_UP_SUCCESS;
    payload: User;
}

type SignUpFailure = {
    type: typeof SIGN_UP_FAILURE;
    payload: string
}

export type UpdateUserAvatar = {
    type: typeof UPDATE_USER_AVATAR,
    payload: string,
}

export type UpdateUserDataRequest = {
    type: typeof UPDATE_USER_DATA_REQUEST;
    payload: {
        fullName: string,
        email: string,
        password: string,
    }
}

type UpdateUserDataSuccess = {
    type: typeof UPDATE_USER_DATA_SUCCESS;
    payload: User;
}

export type UserActionTypes =
    | SetCurrentUser
    | EmailSignInStart
    | SignInSuccess
    | SignInFailure
    | SignOutStart
    | SignOutSuccess
    | SignOutFailure
    | SignUpStart
    | SignUpSuccess
    | SignUpFailure
    | UpdateUserAvatar
    | UpdateUserDataRequest
    | UpdateUserDataSuccess
