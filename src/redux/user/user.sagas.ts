/* eslint-disable no-unused-vars */
// Redux-Saga
import {
  take,
  takeLatest,
  put,
  all,
  call,
  select,
  delay,
} from "redux-saga/effects";

// Types
import {
  EMAIL_SIGN_IN_START,
  SIGN_UP_START,
  SIGN_UP_SUCCESS,
  SIGN_OUT_START,
  UPDATE_USER_AVATAR,
  UPDATE_USER_DATA_REQUEST,
  EmailSignInStart,
  UpdateUserDataRequest,
  SignUpStart,
// eslint-disable-next-line import/no-duplicates
} from "./user.types";

// Actions
import {
  signInSuccess,
  signInFailure,
  signOutSuccess,
  signUpSuccess,
  signUpFailure,
  updateUserDataSuccess,
} from "./user.actions";

import { resetCardsReducer } from "../cards/cards.actions";

import { resetWalletReducer } from "../wallet/wallet.actions";

/* Utils */
import {
  createUser,
  logUser,
  updateDataFor,
} from "../../utils/database/database-user.utils";

import {
  saveUserAvatar,
  getAvatarForUser,
} from "../../utils/storage/storage.utils";

import { createWalletFor } from "../../utils/database/database-wallet.utils";

/* Components */
import Notification, {NOTIFACTION_BG} from "../../components/generic/notification/notification.component";

/* Interfaces */
import IRootState from "../root/root.types";
// eslint-disable-next-line import/no-duplicates
import { User } from "./user.types";
import { UserDB } from '../../database/Users.database';


/* Saga workers */
export function* signInWithEmail({ payload: { email, password } }: EmailSignInStart) {
  try {
    const user: User = yield call(logUser, email, password);

    // On va essayer de recuperer l"avatar de l"utilisateur
    const userAvatar = yield call(getAvatarForUser, user.id);

    if (userAvatar) {
      user.avatar = userAvatar;
    }

    yield put(signInSuccess(user));
  } catch (err) {
    yield put(signInFailure(err));
  }
}

export function* signUp({ payload: { email, password, displayName } }: SignUpStart) {
  try {
    const user = yield call(createUser, displayName, email, password);

    Notification.instance.show(NOTIFACTION_BG.SUCCESS, "Connnexion réussie 🎉");
    yield put(signUpSuccess(user));
  } catch (err) {
    yield put(signUpFailure(err));
  }
}

export function* signOut() {
  yield put(signOutSuccess());
  yield put(resetCardsReducer());
  yield put(resetWalletReducer());
}

export function* updateAvatar() {

 /*
    On va egalement sauvegarder la photo dans le local
    storage vu qu'on nous autorise pas a la sauvegarder
    dans la base de donnees
 */
  const state: IRootState = yield select();

  const user = state.userReducer.currentUser!;

  saveUserAvatar(user.id, user.avatar!);
}

function* updateUserData({payload: {fullName, email, password}}: UpdateUserDataRequest) {
  //On recupere l'id de notre utilisateur
  const user: User = yield select((state: IRootState) => state.userReducer.currentUser);

  //On met a jour la DB
  const [firstName, lastName] = fullName.split(" ");

  const updatedUser: UserDB = yield call(updateDataFor, user.id, firstName, lastName, email, password);

  //On met a jour notre reducer
  const userDataUpdated: User = {
    ...user,
    first_name: updatedUser.first_name,
    last_name: updatedUser.last_name,
    email: updatedUser.email,
    password: updatedUser.password,
  }

  yield put(updateUserDataSuccess(userDataUpdated));
}

/* Saga Watchers */
export function* onEmailSignInStart() {
  yield takeLatest(EMAIL_SIGN_IN_START, signInWithEmail);
}

export function* onSignOutStart() {
  yield takeLatest(SIGN_OUT_START, signOut);
}

export function* onSignUpStart() {
  yield takeLatest(SIGN_UP_START, signUp);
}

export function* onSignUpSuccess() {
  while (true) {
    yield take(SIGN_UP_SUCCESS);

    // On doit recuperer l'id de l'utilisateur
    const user: User = yield select((state: IRootState) => state.userReducer.currentUser);

    // On va maintenant creer le wallet pour l'utilisateur
    yield call(createWalletFor, user.id);
  }
}

export function* onUpdateAvatar() {
  yield takeLatest(UPDATE_USER_AVATAR, updateAvatar);
}

export function* onUpdateUserDataRequest() {
  yield takeLatest(UPDATE_USER_DATA_REQUEST, updateUserData);
}
export default function* userSagas() {
  yield all([
    call(onEmailSignInStart),
    call(onSignOutStart),
    call(onSignUpStart),
    call(onSignUpSuccess),
    call(onUpdateAvatar),
    call(onUpdateUserDataRequest),
  ]);
}
