/* eslint-disable no-unused-vars */
// Types
import {
    User,
    EMAIL_SIGN_IN_START,
    SIGN_IN_SUCCESS,
    SIGN_IN_FAILURE,
    SIGN_UP_START,
    SIGN_UP_SUCCESS,
    SIGN_UP_FAILURE,
    SIGN_OUT_START,
    SIGN_OUT_SUCCESS,
    SIGN_OUT_FAILURE,
    UPDATE_USER_AVATAR,
    UPDATE_USER_DATA_REQUEST,
    UPDATE_USER_DATA_SUCCESS,
    UserActionTypes,
} from "./user.types";

// Action Types
export type emailSignInType = {
  (email: string, password: string): UserActionTypes;
}

export type signUpType = {
  (displayName: string, email: string, password: string): UserActionTypes;
}

export type signOutType = {
  (): UserActionTypes;
}

export type updateUserAvatarType = {
  (file: File): UserActionTypes;
}

export type updateUserDataRequestType = {
  (fullName: string, email: string, password: string): UserActionTypes;
} 

// Actions
export const emailSignInStart = (email: string, password: string) : UserActionTypes => ({
  type: EMAIL_SIGN_IN_START,
  payload: {
      email,
      password,
  },
});

export const signInSuccess = (user: User) : UserActionTypes => ({
  type: SIGN_IN_SUCCESS,
  payload: user,
});

export const signInFailure = (error: string): UserActionTypes => ({
  type: SIGN_IN_FAILURE,
  payload: error,
});

export const signOutStart = () : UserActionTypes => ({ type: SIGN_OUT_START });

export const signOutSuccess = () : UserActionTypes => ({ type: SIGN_OUT_SUCCESS });

export const signOutFailure = (error: string): UserActionTypes => ({
  type: SIGN_OUT_FAILURE,
  payload: error,
});

export const signUpStart = (
  displayName: string,
  email: string,
  password: string,
  ): UserActionTypes => ({
  type: SIGN_UP_START,
  payload: {
      displayName,
      email,
      password,
  },
});

export const signUpSuccess = (user: User): UserActionTypes => ({
  type: SIGN_UP_SUCCESS,
  payload: user,
});

export const signUpFailure = (error: string) => ({
  type: SIGN_UP_FAILURE,
  payload: error,
});

export const updateUserAvatar = (file: File): UserActionTypes => ({
  type: UPDATE_USER_AVATAR,
  payload: URL.createObjectURL(file),
});

export const updateUserDataRequest = (fullName: string, email: string, password: string): UserActionTypes => ({
  type: UPDATE_USER_DATA_REQUEST,
  payload: {
    fullName,
    email,
    password,
  }
});

export const updateUserDataSuccess = (user: User): UserActionTypes => ({
  type: UPDATE_USER_DATA_SUCCESS,
  payload: user,
});