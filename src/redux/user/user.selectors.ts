
import { createSelector } from "reselect";

/* Utils */
// CryptoJs
import CryptoJS from "crypto-js";

import IRootState from "../root/root.types";
import { User } from "./user.types";

const selectUser = (state: IRootState) => state.userReducer;

export const selectCurrentUser = createSelector(
  [selectUser],
  userReducer => userReducer.currentUser,
);

export const selectCurrentUserFullName = createSelector(
  [selectCurrentUser],
  (user) => {
    if(!user) return "";
    
    return `${user.first_name} ${user.last_name}`;
  }
);

export const selectCurrentUserEmail = createSelector(
  [selectCurrentUser],
  (user) => {
    if(!user) return "";

    return user.email;
  }
);

export const selectCurrentUserPassword = createSelector(
  [selectCurrentUser],
  user => {
    if(!user) return "";
    
    return CryptoJS.DES.decrypt(user.password, "hugo").toString(CryptoJS.enc.Utf8);
  }
)

export const selectSignInFailure = createSelector(
  [selectUser],
  userReducer => userReducer.signInError,
);

export const selectSignUpFailure = createSelector(
  [selectUser],
  userReducer => userReducer.signUpError,
);
