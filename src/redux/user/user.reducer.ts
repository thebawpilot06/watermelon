/* eslint-disable no-unused-vars */
// Types
import {
  User,
  IUserState,
  EMAIL_SIGN_IN_START,
  SIGN_UP_START,
  SIGN_IN_SUCCESS,
  SIGN_IN_FAILURE,
  SIGN_UP_SUCCESS,
  SIGN_UP_FAILURE,
  SIGN_OUT_SUCCESS,
  SIGN_OUT_FAILURE,
  UPDATE_USER_AVATAR,
  UPDATE_USER_DATA_SUCCESS,
  UserActionTypes,
} from "./user.types";

// Initial State
const INITIAL_STATE: IUserState = {
    currentUser: null,
    signInError: null,
    signUpError: null,
    signOutError: null,
};

const userReducer = (state: IUserState = INITIAL_STATE, action: UserActionTypes): IUserState => {
    switch (action.type) {
        case EMAIL_SIGN_IN_START:
            return {
                ...state,
                signInError: null,
            };
        case SIGN_UP_START:
            return {
                ...state,
                signUpError: null,
            };
        case SIGN_IN_SUCCESS:
            return {
                ...state,
                currentUser: action.payload,
                signInError: null,
            };
        case SIGN_UP_SUCCESS:
            return {
                ...state,
                currentUser: action.payload,
                signUpError: null,
            };
        case SIGN_OUT_SUCCESS:
            return {
                ...state,
                currentUser: null,
                signOutError: null,
            };
        case SIGN_IN_FAILURE:
            return {
                ...state,
                signInError: action.payload,
            };
        case SIGN_UP_FAILURE:
            return {
                ...state,
                signUpError: action.payload,
            };
        case SIGN_OUT_FAILURE:
            return {
                ...state,
                signOutError: action.payload,
            };
        case UPDATE_USER_AVATAR:
            return {
                ...state,
                currentUser: {
                    ...state.currentUser,
                    avatar: action.payload,
                } as User,
            };
        case UPDATE_USER_DATA_SUCCESS:
            return {
                ...state,
                currentUser: action.payload,
            };
        default:
            return state;
    }
};

export default userReducer;
