/* eslint-disable no-unused-vars */
import { createSelector } from "reselect";

/* Utils */
// Lodash
import { isEmpty } from "lodash";

// Date
import { convertDateToMMYY } from "../../utils/date/date.utils";

/* Interfaces */
import IRootState from "../root/root.types";
import { CardData } from "../../ressources/interfaces/generic.interfaces";

const selectCardsReducer = (state: IRootState) => state.cardsReducer;

export const selectAreCardsFetching = createSelector(
    [selectCardsReducer],
    (cardsReducer) => cardsReducer.cardsFetching,
);

export const selectCards = createSelector(
    [selectCardsReducer],
    (cardsReducer) => {
    // On regarde tout d'abord si le champ card est null
        if (isEmpty(cardsReducer.cards)) return null;

    // Maintenant on va preparer les cards pour qu' elles matches les attentes de notre composant

        const cards: {[key: string]: CardData} = { };

      // eslint-disable-next-line guard-for-in,no-restricted-syntax
        for (const card in cardsReducer.cards) {
            const expiration = cardsReducer.cards[card].expiredAt;

            const expirationString = convertDateToMMYY(expiration);

            cards[card] = {
                cardId: cardsReducer.cards[card].id,
                cardNumber: cardsReducer.cards[card].last_four,
                cardExpiration: expirationString,
                cardColor: cardsReducer.cards[card].cardColor,
                cardBrand: cardsReducer.cards[card].brand,
            };
        }

        return cards;
    },
);

export const selectCardError = createSelector(
    [selectCardsReducer],
    (cardReducer) => cardReducer.error,
);
