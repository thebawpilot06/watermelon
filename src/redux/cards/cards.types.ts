/* eslint-disable no-unused-vars */
import { BrandName } from "../../database/Cards.database";

export type CardInput = {
    last_four: string;
    cardColor: string;
    brand: BrandName;
    expiredAt: Date;
}

export type Card = CardInput & {
    id: number;
}

export enum CardError {
    CARD_ALREADY_EXISTS = "CARD_ALREADY_EXISTS",
}

export type Cards = {
    [idCard: string]: Card
}

export interface ICardsState {
    cards: Cards | null;
    cardsFetching: boolean;
    error: CardError | null;
    expiration: Date | null;
}

// Action Types
export const FETCH_CARDS_REQUEST = "FETCH_CARDS_REQUEST";
export const FETCH_CARDS_START = "FETCH_CARDS_START";
export const FETCH_CARDS_SUCCESS = "FETCH_CARDS_SUCCESS";

export const ADD_CARD_REQUEST = "ADD_CARD_REQUEST";
export const ADD_CARD_SUCCESS = "ADD_CARD_SUCCESS";
export const ADD_CARD_FAILURE = "ADD_CARD_FAILURE";
export const EDIT_CARD_REQUEST = "EDIT_CARD_REQUEST";
export const EDIT_CARD_SUCCESS = "EDIT_CARD_SUCCESS";
export const EDIT_CARD_FAILURE = "EDIT_CARD_FAILURE";
export const DELETE_CARD = "DELETE_CARD";

export const RESET_CARDS_REDUCER = "RESET_CARDS_REDUCER";

// Action Interfaces
export type FetchCardsRequest = {
    type: typeof FETCH_CARDS_REQUEST,
}

export type FetchCardsStart = {
    type: typeof FETCH_CARDS_START;
}

export type FetchCardsSuccess = {
    type: typeof FETCH_CARDS_SUCCESS;
    payload: {
            cards: {
                [id: string]: Card;
            },
            expiration: Date;
        }
}

export type AddCardRequest = {
    type: typeof ADD_CARD_REQUEST;
    payload: CardInput;
}

export type AddCardSuccess = {
    type: typeof ADD_CARD_SUCCESS;
    payload: Card;
}

export type AddCardFailure = {
    type: typeof ADD_CARD_FAILURE;
    payload: CardError;
}

export type EditCardRequest = {
    type: typeof EDIT_CARD_REQUEST;
    payload: Card;
}

export type EditCardSuccess = {
    type: typeof EDIT_CARD_SUCCESS;
    payload: Card,
}

export type EditCardFailure = {
    type: typeof EDIT_CARD_FAILURE;
    payload: CardError;
}

export type DeleteCard = {
    type: typeof DELETE_CARD;
    payload: number;
}

export type ResetCardsReducer = {
    type: typeof RESET_CARDS_REDUCER;
}

export type CardsActionTypes =
    | FetchCardsRequest
    | FetchCardsStart
    | FetchCardsSuccess
    | AddCardRequest
    | AddCardSuccess
    | AddCardFailure
    | EditCardRequest
    | EditCardSuccess
    | EditCardFailure
    | DeleteCard
    | ResetCardsReducer
