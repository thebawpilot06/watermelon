/* eslint-disable no-unused-vars */
/* Types */
import {
    ICardsState,
    FETCH_CARDS_START,
    FETCH_CARDS_SUCCESS,
    ADD_CARD_REQUEST,
    ADD_CARD_SUCCESS,
    ADD_CARD_FAILURE,
    EDIT_CARD_REQUEST,
    EDIT_CARD_SUCCESS,
    EDIT_CARD_FAILURE,
    DELETE_CARD,
    RESET_CARDS_REDUCER,
    CardsActionTypes,
} from "./cards.types";

/* Utils */
import {
    removeCard,
    updateCard,
} from "./card.utils";

/* Initial State */
const INITIAL_STATE: ICardsState = {
    cards: null,
    cardsFetching: false,
    error: null,
    expiration: null,
};

// eslint-disable-next-line max-len
const cardsReducer = (state: ICardsState = INITIAL_STATE, action: CardsActionTypes): ICardsState => {
    switch (action.type) {
    case FETCH_CARDS_START:
        return {
            ...state,
            cardsFetching: true,
        };
    case FETCH_CARDS_SUCCESS:
        return {
            ...state,
            cards: action.payload.cards,
            cardsFetching: false,
            expiration: action.payload.expiration,
        };
    case "ADD_CARD_REQUEST":
    case "EDIT_CARD_REQUEST":
        return {
            ...state,
            error: null,
        };
    case "ADD_CARD_SUCCESS": {
        return {
            ...state,
            cards: {
                ...state.cards,
                [action.payload.id]: action.payload,
            },
            error: null,
        };
    }
    case "EDIT_CARD_SUCCESS":
        return {
            ...state,
            cards: updateCard(action.payload, state.cards!),
            error: null,
        };
    case "ADD_CARD_FAILURE":
    case "EDIT_CARD_FAILURE":
        return {
            ...state,
            error: action.payload,
        };
    case DELETE_CARD:
        return {
            ...state,
            cards: removeCard(action.payload, state.cards!),
        };
    case RESET_CARDS_REDUCER:
        return { ...INITIAL_STATE };
    default:
        return state;
    }
};

export default cardsReducer;
