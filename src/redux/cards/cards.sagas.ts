/* eslint-disable no-unused-vars */
// Redux-Saga
import {
    all, call, delay, put, select, take, takeLatest,
} from "redux-saga/effects";
// Types
// Cards
import {
    ADD_CARD_REQUEST,
    AddCardRequest,
    CardError,
    Cards,
    DELETE_CARD,
    DeleteCard,
    EDIT_CARD_REQUEST,
    EditCardRequest,
    FETCH_CARDS_REQUEST,
// eslint-disable-next-line import/no-duplicates
} from "./cards.types";
// Actions
import {
    addCardFailure,
    addCardSuccess,
    editCardFailure,
    editCardSuccess,
    fetchCardsStart,
    fetchCardsSuccess,
} from "./cards.actions";

/* Utils */
// Date
import {
    checkExpirationPassed,
    setExpiration,
} from "../../utils/date/date.utils";

// Cards
import { getColorForCard, saveCardColor } from "../../utils/storage/cards-storage.utils";

import {
    addCardToDb,
    checkCardExistence, editCardInDb,
    getCardsFor,
    removeCardFromDb,
} from "../../utils/database/database-cards.utils";

/* Interfaces */
import IRootState from "../root/root.types";

import { CardDB } from "../../database/Cards.database";

// eslint-disable-next-line import/no-duplicates
import { ICardsState, Card } from './cards.types';
import { IUserState, User } from "../user/user.types";

/* Saga workers */
export function* fetchCards() {
    // On va verifier si l"expiration est passee
    const cardsReducer: ICardsState = yield select((state: IRootState) => state.cardsReducer);

    if (!checkExpirationPassed(cardsReducer.expiration)) return;

    // On va indiquer qu'on est en train de charger les cards
    yield put(fetchCardsStart());

    // On attend 800ms pour etre sure que la base de donnees a ete chargee
    yield delay(500);

    // On commence par recuperer l'id de notre utilisateur
    const userReducer: IUserState = yield select((state: IRootState) => state.userReducer);

    // On recupere les cartes de l'utilisateur
    const cardsFromDB: CardDB[] = yield call(getCardsFor, userReducer.currentUser!.id);

    // On va retravailler les cartes pour qu'elles puissent matcher le modele de notre reducer
    const cardsForReducer: Cards = { };

    // eslint-disable-next-line no-restricted-syntax
    for (const card of cardsFromDB) {
        // On va recuperer la couleur de la carte dans le local storage
        const cardColor = getColorForCard(card.id);
        cardsForReducer[card.id] = {
            id: card.id,
            last_four: card.last_four,
            cardColor,
            brand: card.brand,
            expiredAt: card.expired_at,
        };
    }

    // On simule une attente de fetch
    yield delay(2200);

    // On va maintenant ajouter les cartes a notre objet current user
    yield put(fetchCardsSuccess(cardsForReducer, setExpiration(30)));
}

export function* addCard({ payload: card }: AddCardRequest) {
// On doit d'abord verifier si une carte avec le meme id n'existe pas deja
    try {
       

        // Nous avons besoin de recuperer l'id de l'utilisateur
        const user: User = yield select(
            (state: IRootState) => state.userReducer.currentUser!,
        );

        yield call(checkCardExistence, user.id, card.last_four, card.brand, card.expiredAt);
        // On ajoute la nouvelle carte dans notre base de donnees
        const {last_four, brand, expiredAt} = card;

        const addedCard: CardDB = yield call(addCardToDb, last_four, brand, expiredAt, user.id);

        // Il faut tout d'abord sauvegarder la couleur de la carte dans le local storage
        yield call(saveCardColor, addedCard.id, card.cardColor);

        const cardForReducer: Card = {
            id: addedCard.id,
            last_four: addedCard.last_four,
            brand: addedCard.brand,
            expiredAt: addedCard.expired_at,
            cardColor: card.cardColor,
        };

        // On rajoute la nouvelle carte a notre reducer
        yield put(addCardSuccess(cardForReducer));
    } catch (err) {
        yield put(addCardFailure(CardError.CARD_ALREADY_EXISTS));
    }
}

export function* editCard({ payload: card }: EditCardRequest) {

    /*
        On va verifier que la modification de la carte n'a pas entrainer
        la recreation d'une carte qui existe deja
     */
    try {
        

        const user: User = yield select((state: IRootState) => state.userReducer.currentUser);

        yield call(checkCardExistence, user.id, card.last_four, card.brand, card.expiredAt);

        // On enregistre la carte modifiee dans notre reducer
        yield put(editCardSuccess(card));

        // On enregistre la nouvelle carte dans la base de donnee
        const {id, last_four, brand, expiredAt} = card;
        yield call(editCardInDb, id, last_four, brand, expiredAt);
    } catch (err) {
        yield put(editCardFailure(CardError.CARD_ALREADY_EXISTS));
    }
}

export function* deleteCard({ payload: cardId }: DeleteCard) {
    yield call(removeCardFromDb, cardId);
}

/* Saga watchers */
export function* onFetchCardsRequest() {
    yield takeLatest(FETCH_CARDS_REQUEST, fetchCards);
}

export function* onAddCardRequest() {
    yield takeLatest(ADD_CARD_REQUEST, addCard);
}

export function* onEditCardRequest() {
    yield takeLatest(EDIT_CARD_REQUEST, editCard);
}

export function* onDeleteCard() {
    yield takeLatest(DELETE_CARD, deleteCard);
}

export default function* cardsSagas() {
    yield all([
        call(onFetchCardsRequest),
        call(onAddCardRequest),
        call(onEditCardRequest),
        call(onDeleteCard),
    ]);
}
