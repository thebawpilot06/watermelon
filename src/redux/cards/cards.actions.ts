/* eslint-disable no-unused-vars */
// Types
import {
    Card,
    CardInput,
    CardError,
    FETCH_CARDS_REQUEST,
    FETCH_CARDS_START,
    FETCH_CARDS_SUCCESS,
    ADD_CARD_REQUEST,
    ADD_CARD_SUCCESS,
    ADD_CARD_FAILURE,
    EDIT_CARD_REQUEST,
    EDIT_CARD_SUCCESS,
    EDIT_CARD_FAILURE,
    DELETE_CARD,
    RESET_CARDS_REDUCER,
    CardsActionTypes,
} from "./cards.types";

// Action Types
export type addCardRequestType = {
  (card: CardInput): CardsActionTypes;
}

export type editCardRequestType = {
  (card: Card): CardsActionTypes;
}

export type deleteCardType = {
  (cardId: number): CardsActionTypes;
}

// Actions
export const fetchCardsRequest = (): CardsActionTypes => ({ type: FETCH_CARDS_REQUEST });

export const fetchCardsStart = (): CardsActionTypes => ({ type: FETCH_CARDS_START });

// eslint-disable-next-line max-len
export const fetchCardsSuccess = (cards: {[id:string]: Card}, expiration: Date): CardsActionTypes => ({
    type: FETCH_CARDS_SUCCESS,
    payload: {
        cards,
        expiration,
    },
});

export const addCardRequest = (card: CardInput): CardsActionTypes => ({
    type: "ADD_CARD_REQUEST",
    payload: card,
});

export const addCardSuccess = (card: Card): CardsActionTypes => ({
    type: "ADD_CARD_SUCCESS",
    payload: card,
});

export const addCardFailure = (cardError: CardError): CardsActionTypes => ({
    type: "ADD_CARD_FAILURE",
    payload: cardError,
});

export const editCardRequest = (card: Card): CardsActionTypes => ({
    type: "EDIT_CARD_REQUEST",
    payload: card,
});

export const editCardSuccess = (card: Card): CardsActionTypes => ({
    type: "EDIT_CARD_SUCCESS",
    payload: card,
});

export const editCardFailure = (cardError: CardError) => ({
    type: EDIT_CARD_FAILURE,
    payload: cardError,
});

export const deleteCard = (cardId: number): CardsActionTypes => ({
    type: DELETE_CARD,
    payload: cardId,
});

export const resetCardsReducer = (): CardsActionTypes => ({ type: RESET_CARDS_REDUCER });
