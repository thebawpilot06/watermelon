/* eslint-disable no-unused-vars */
/* Interfaces */
import {
    Card,
    Cards,
} from "./cards.types";

export const removeCard = (cardId: number, cards: Cards) => {
    const newCards = { ...cards };

    // On supprime l'element que l'on ne veut plus
    delete newCards[cardId];
    return newCards;
};

export const updateCard = (newCard: Card, cards: Cards) => {
    const newCards = { ...cards };

    newCards[newCard.id] = newCard;

    return newCards;
};
