// Redux
import {
  createStore,
  applyMiddleware,
  compose,
} from "redux";
import { persistStore } from "redux-persist";
import createSagaMiddleware from "redux-saga";
import logger from "redux-logger";

// Import des root
import rootReducer from "./root/root-reducer";
import rootSaga from "./root/root-saga";

const sagaMiddleware = createSagaMiddleware();

declare global {
  interface Window {
    // eslint-disable-next-line no-undef
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middlewares: any[] = [
	sagaMiddleware,
];

// On n"ajoute logger qu"en dev pas en production
if (process.env.NODE_ENV === "development") middlewares.push(logger);

// Creation du store redux
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(...middlewares)));

// On lance l'ecoute des sagas
sagaMiddleware.run(rootSaga);

// On cree notre persistor
const persistor = persistStore(store);

export { store, persistor };
