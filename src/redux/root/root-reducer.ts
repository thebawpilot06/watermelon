// Redux
import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

// Import des reducers
import userReducer from "../user/user.reducer";
import walletReducer from "../wallet/wallet.reducer";
import cardsReducer from "../cards/cards.reducer";
//----

const persistConfig = {
	key: "root",
	storage, // On enregistre toutes nos donnees dans le local storage
	whitelist: [
		"userReducer",
		"walletReducer",
		"cardsReducer",
	],
	// Dans ce tableau nous mettrons tous les reducers que l'on souhaite sauvegarder
	// dans le local storage
};

export const rootReducer = combineReducers({
	userReducer,
	walletReducer,
	cardsReducer,
});

export default persistReducer(persistConfig, rootReducer);
