// Redux Saga
import { all, call } from "redux-saga/effects";

// Sagas
import userSagas from "../user/user.sagas";
import walletSagas from "../wallet/wallet.sagas";
import cardsSagas from "../cards/cards.sagas";

export default function* rootSaga() {
	yield all([
		call(userSagas),
		call(walletSagas),
		call(cardsSagas),
	]);
}
