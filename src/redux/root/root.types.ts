/* eslint-disable no-unused-vars */
// Reducers
import { IUserState } from "../user/user.types";
import { IWalletState } from "../wallet/wallet.types";
import { ICardsState } from "../cards/cards.types";

export default interface IRootState {
    userReducer: IUserState,
    walletReducer: IWalletState,
    cardsReducer: ICardsState,
// eslint-disable-next-line semi
}
