//Redux
import {combineReducers} from 'redux';
import {persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

//Import des reducers

//----

const persistConfig = {
    key: 'root',
    storage, //On enregistre toutes nos donnees dans le local storage
    whitelist: [] //Dans ce tableau nous mettrons tous les reducers que l'on souhaite sauvegarder dans le local storage
};

export const rootReducer = combineReducers({});

export default persistReducer(persistConfig, rootReducer);


